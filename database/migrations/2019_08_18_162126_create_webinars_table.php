<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Webinar;

class CreateWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinars', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title', 255);
          $table->text('md')->nullable();
          $table->text('html')->nullable();
          $table->string('image', 255)->nullable();
          $table->string('slug', 255)->unique();

          $table->enum('status', Webinar::$statuses)->default(Webinar::STATUS_DRAFT);

          $table->date('date');
          $table->time('start_at')->nullable();
          $table->time('end_at')->nullable();

          $table->string('caption', 255)->nullable();

          $table->unsignedDecimal('cost',13,2)->nullable();

          $table->dateTime('published_at')->nullable();

          $table->string('og_image')->nullable();
          $table->string('seo_title')->nullable();
          $table->text('meta_description')->nullable();
          $table->text('meta_keywords')->nullable();

          $table->unsignedInteger('created_by');
          $table->unsignedInteger('updated_by')->nullable();
          $table->softDeletes();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinars');
    }
}
