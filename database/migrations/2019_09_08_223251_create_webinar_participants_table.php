<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebinarParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinar_participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference', 25)->unique();
            $table->string('status', 50);
            $table->unsignedInteger('webinar_id');
            $table->unsignedDecimal('total_price',13,2);
            $table->string('firstname',255);
            $table->string('lastname',255);
            $table->string('email',255);
            $table->string('phone',255);
            $table->string('promocode',25)->nullable();
            $table->string('city',255)->nullable();
            $table->boolean('is_made_call')->default(false);
            $table->boolean('is_answered_call')->default(false);
            $table->text('comment')->nullable();
            $table->text('notation')->nullable();
            $table->timestamp('payment_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('webinar_id')
            ->references('id')->on('webinars')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinar_participants');
    }
}
