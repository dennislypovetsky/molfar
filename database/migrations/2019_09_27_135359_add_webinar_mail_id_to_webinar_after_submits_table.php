<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebinarMailIdToWebinarAfterSubmitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webinar_after_submits', function (Blueprint $table) {
          $table->unsignedInteger('webinar_mail_id')->nullable()->after('webinar_id');
          $table->foreign('webinar_mail_id')
            ->references('id')->on('webinar_mails')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webinar_after_submits', function (Blueprint $table) {
            //
        });
    }
}
