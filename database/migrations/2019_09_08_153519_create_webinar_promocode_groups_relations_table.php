<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebinarPromocodeGroupsRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinar_promocode_groups_relations', function (Blueprint $table) {
          $table->unsignedInteger('promocode_id');
          $table->unsignedInteger('group_id');

          $table->primary(['promocode_id','group_id']);

          $table->foreign('promocode_id')
          ->references('id')->on('webinar_promocodes')
          ->onDelete('cascade');
          $table->foreign('group_id')
          ->references('id')->on('webinar_promocode_groups')
          ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinar_promocode_groups_relations');
    }
}
