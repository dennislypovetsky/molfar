<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\WebinarAfterSubmit;

class CreateWebinarAfterSubmitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinar_after_submits', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('webinar_id');
            $table->unsignedInteger('promocode_group_id')->nullable();
            $table->enum('discount_type', WebinarAfterSubmit::$discountTypes)->nullable();
            $table->decimal('discount_amount',13,2)->nullable();

            $table->timestamps();

            $table->foreign('webinar_id')
            ->references('id')->on('webinars')
            ->onDelete('cascade');

            $table->foreign('promocode_group_id')
            ->references('id')->on('webinar_promocode_groups')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinar_after_submits');
    }
}
