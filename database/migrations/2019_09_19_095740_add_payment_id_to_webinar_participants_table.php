<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentIdToWebinarParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webinar_participants', function (Blueprint $table) {
          $table->unsignedBigInteger('payment_id')->nullable()->after('notation');
          $table->foreign('payment_id')
            ->references('id')->on('webinar_payments')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webinar_participants', function (Blueprint $table) {
            //
        });
    }
}
