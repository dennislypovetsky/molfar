<?php
  return [
    'hero__title' => 'How it was in 2019',
    'hero__description' => 'The debut in the 18th was good.
    But judged by the second album',

    'blocks__title--visually-hidden' => 'Photo memories from Molfar Beauty Forum ‘19',

    'blocks__title--1' => 'Master class from Lakme',
    'blocks__title--2' => 'Seminar from Hair Company',
    'blocks__title--3' => 'Textural makeup by Maxim Gilyov',
    'blocks__title--4' => 'Chinese gel painting by Tatyana Solovyova',
    'blocks__title--5' => 'Styling and coloring eyebrows by Elan',
    'blocks__title--6' => 'Sergey Kormakov told how to increase sales using promotions',
    'blocks__title--7' => 'Haircut by Manuel Leale',
    'blocks__title--8' => 'Route 66 by Hairgum',
    'blocks__title--9' => 'Seminar by Keen “Neo Babies”',
    'blocks__title--10' => 'Dream Team competition',
    'blocks__title--11' => 'Night shows and parties',

    'blocks__alt--1' => 'Emotions and new experiences',
    'blocks__alt--2' => 'Brandwall',
    'blocks__alt--3' => 'Brand exhibition',

    'blocks__advertising' => 'See full photo report <br>on our Drive',
    'blocks__link' => 'See all photos',
  ];
