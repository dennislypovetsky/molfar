<?php
  return [
    'main'=>'Main',
    'schedule'=>'Schedule',
    'recreation'=>'Recreation',
    'dream-team'=>'Dream Team',
    'brow-pro'=>'Brow Pro',
    'miss-molfar'=>'Miss Molfar',
    'golden-hands'=>'Golden Hands',
    'foot-profi'=>'Foot Profi',
    'vote'=>'Vote',
    'news'=>'News',
    'privacy'=>'Privacy policy',
    'kontora-design'=>'Made in “Kontora”',
  ];
