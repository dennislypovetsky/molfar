<?php
  return [
    'hero_title_hidden' => 'Molfar Beauty Forum ‘:year',
    'hero_title' => 'Main <br>beauty conference <br>in Ukraine',
    'throwback_caption' => 'See how cool it was in :year',
    'buy_ticket' => 'Purchase a ticket'
  ];
