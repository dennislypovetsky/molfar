<?php
  return [
    'main'=>'Главная',
    'schedule'=>'Программа',
    'recreation'=>'Отдых',
    'dream-team'=>'Dream Team',
    'brow-pro'=>'Brow Pro',
    'miss-molfar'=>'Miss Molfar',
    'golden-hands'=>'Золотые руки',
    'foot-profi'=>'Foot Profi',
    'vote'=>'Голосование',
    'news'=>'Новости',
    'privacy'=>'Политика конфиденциальности',
    'kontora-design'=>'Сделано в «Дизайн-Конторе»',
  ];
