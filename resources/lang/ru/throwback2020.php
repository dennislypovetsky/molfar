<?php
  return [
    'hero__title' => 'Как это было в 2019 году',
    'hero__description' => 'Дебют в 18-м был хорош. Но судят по второму альбому',

    'blocks__title--visually-hidden' => 'Фото-воспоминания Molfar Beauty Forum ‘19',

    'blocks__title--1' => 'Мастер-класс от Lakme',
    'blocks__title--2' => 'Семинар от компании Hair Company',
    'blocks__title--3' => 'Текстурный макияж от Максима Гилёва',
    'blocks__title--4' => 'Китайская роспись гелями от Татьяны Соловьёвой',
    'blocks__title--5' => 'Моделирование и окрашивание бровей от Elan',
    'blocks__title--6' => 'Сергей Кормаков рассказал как увеличить продажи, используя акции',
    'blocks__title--7' => 'Стрижка от Мануэля Леале',
    'blocks__title--8' => 'Route 66 от Hairgum',
    'blocks__title--9' => 'Семинар Keen Neo Babies',
    'blocks__title--10' => 'Командный конкурс Dream Team',
    'blocks__title--11' => 'Ночные шоу и вечеринки',

    'blocks__alt--1' => 'Эмоции и новые впечатления',
    'blocks__alt--2' => 'Брендвол',
    'blocks__alt--3' => 'Выставка брендов',

    'blocks__advertising' => 'Полный фотоотчёт <br>смотрите на нашем Драйве',
    'blocks__link' => 'Смотреть все фото',
  ];
