<?php
  return [
    'hero_title_hidden' => 'Molfar Beauty Forum ‘:year',
    'hero_title' => 'Главная <br>бьюти-конференция в Украине',
    'throwback_caption' => 'Смотреть как было круто в :year году',
    'buy_ticket' => 'Приобрести билет'
  ];
