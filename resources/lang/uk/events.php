<?php
  return [
    'hero_title_hidden' => 'Molfar Beauty Forum ‘:year',
    'hero_title' => 'Головна <br>б’юті-конференція в Україні',
    'throwback_caption' => 'Дивитися як було круто в :year році',
    'buy_ticket' => 'Придбати квиток'
  ];
