<?php
  return [
    'main'=>'Головна',
    'schedule'=>'Програма',
    'recreation'=>'Відпочинок',
    'dream-team'=>'Dream Team',
    'brow-pro'=>'Brow Pro',
    'miss-molfar'=>'Miss Molfar',
    'golden-hands'=>'Золоті руки',
    'foot-profi'=>'Foot Profi',
    'vote'=>'Голосування',
    'news'=>'Новини',
    'privacy'=>'Політика конфіденційності',
    'kontora-design'=>'Зроблено у «Дизайн-Конторі»'
  ];
