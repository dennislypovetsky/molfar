<?php
  return [
    'PUBLISHED' => 'Опубликовано',
    'DRAFT' => 'Черновик',
    'PENDING' => 'В обработке',
  ];
