<?php
  return [
    'hero__title' => 'Як це було у 2019 році',
    'hero__description' => 'Дебют у 18-м був гарний. Але судять по другому альбому',

    'blocks__title--visually-hidden' => 'Фото-спогади Molfar Beauty Forum ‘19',

    'blocks__title--1' => 'Майстер-клас від Lakme',
    'blocks__title--2' => 'Семінар від компанії Hair Company',
    'blocks__title--3' => 'Текстурний макіяж від Максима Гільова',
    'blocks__title--4' => 'Китайська розпис гелями від Тетяни Соловйової',
    'blocks__title--5' => 'Моделювання та фарбування брів від Elan',
    'blocks__title--6' => 'Сергій Кормаков розповів як збільшити продажі, використовуючи акції',
    'blocks__title--7' => 'Стрижка від Мануеля Леале',
    'blocks__title--8' => 'Route 66 від Hairgum',
    'blocks__title--9' => 'Семінар Keen Neo Babies',
    'blocks__title--10' => 'Командний конкурс Dream Team',
    'blocks__title--11' => 'Нічні шоу і вечірки',

    'blocks__alt--1' => 'Емоції і нові враження',
    'blocks__alt--2' => 'Брендвол',
    'blocks__alt--3' => 'Виставка брендів',

    'blocks__advertising' => 'Повний фотозвіт <br>дивіться на нашому Драйві',
    'blocks__link' => 'Дивитися всі фото',
  ];
