var Validation = (function(){

  var input_types = ['text','tel','email'];


  function Validation(form, options){

    this.form = form;
    this.options = options;

    this.messages= {};

    this.inputs_count=0;
    this.validated_inputs=[];

    this.init();

  }

  Validation.prototype.defaults = {

    parent_selector: '*',
    cls_warning: '',
    cls_danger: '',
    cls_hint: '',
    cls_mistake: '',
    passed:null

  };

  Validation.prototype.init=function(){
    var options = this.options;

    this.options = Object.assign({}, this.defaults, options);

    this.handleInputs();

  };

  function onInvalid(validation){

    return function(e){

      e.preventDefault();

      validation.onInvalid(this);
    };
  }

  function onInput(validation) {

    return function (e) {

      e.preventDefault();

      validation.onInput(this);
    };
  }

  function findNextSibling(elem, filter){

    while (elem = elem.nextSibling) {
      if (elem.nodeType === 3) continue; // text node
      if (!filter || filter(elem)) return elem;
    }

    return false;

  }

  Validation.prototype.onInvalid = function(elem){
    var cls_mistake = this.options.cls_mistake,
      parent = elem.closest(this.options.parent_selector);

    parent.classList.add(this.options.cls_danger);

    if(
      !findNextSibling(elem, function (sibling) {
        return sibling.classList.contains(cls_mistake);
      })
    ){

      elem.insertAdjacentHTML('afterend', '<span class="' + cls_mistake + '">' + this.messages[elem.name] + '</span>');
      // elem.insertAdjacentHTML('afterend', '<span class="'+cls_mistake+'">' + elem.dataset.invalidMsg + '</span>');

    }

    this.inputInvalid(elem);


  };

  Validation.prototype.setMessage = function(name, message){
    this.messages[name] = message;
  };


  Validation.prototype.inputInvalid = function(elem){
    var name = elem.name, index;
    index = this.validated_inputs.indexOf(name);

    if(index > -1){
      this.validated_inputs.splice(index,1);
    }
  };


  Validation.prototype.inputPassed = function(elem){
    var name=elem.name;

    if (!this.validated_inputs.includes(name)) {

      this.validated_inputs.push(name);


      if (this.validated_inputs.length == this.inputs_count){
        if(this.options.passed){
          this.options.passed();
        }
      }

    }

  };

  Validation.prototype.onInput = function (elem) {

     var cls_mistake = this.options.cls_mistake,
      is_valid = elem.validity.valid,
      parent = elem.closest(this.options.parent_selector);

     if(is_valid){

       findNextSibling(elem, function (sibling) {

         if(sibling.classList.contains(cls_mistake)){
          sibling.parentNode.removeChild(sibling);
          return true;
         }

       });

       parent.classList.remove(this.options.cls_warning, this.options.cls_danger);

       this.inputPassed(elem);

     }
  };


  Validation.prototype.handleInputs = function () {
    var form=this.form, elements=form.elements,
      i=0, len=elements.length, element;

    for (; i<len; i++) {
      element=elements[i];

      if (element.nodeName === "INPUT" && input_types.includes(element.type) && element.required) {

        this.inputs_count+=1;

        this.messages[element.name] = element.dataset.invalidMsg || element.validationMessage;

        element.addEventListener('invalid', onInvalid(this),true);
        element.addEventListener('input', onInput(this), true);

      }
    }

  };

//Polyfils
if (!Element.prototype.matches)
  Element.prototype.matches = Element.prototype.msMatchesSelector ||
  Element.prototype.webkitMatchesSelector;

if (!Element.prototype.closest)
  Element.prototype.closest = function (s) {
    var el = this;
    if (!document.documentElement.contains(el)) return null;
    do {
      if (el.matches(s)) return el;
      el = el.parentElement || el.parentNode;
    } while (el !== null && el.nodeType === 1);
    return null;
};
if (typeof Object.assign != 'function') {
  // Must be writable: true, enumerable: false, configurable: true
  Object.defineProperty(Object, "assign", {
    value: function assign(target, varArgs) { // .length of function is 2
      'use strict';
      if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable: true,
    configurable: true
  });
}



  return Validation;

})();
