<?php
require_once('../vendor/PHPMailer/vendor/autoload.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$email = new PHPMailer();
$email->SetFrom('inbox@molfarforum.com', 'Molfar');
$email->CharSet  = "UTF-8";
$email->Encoding = 'base64';
$subject = $_POST['firstname']." ".$_POST['lastname']." — Miss Molfar 2022";
$body = "";

if (isset($_POST["firstname"]) && !empty($_POST["firstname"]))
    $body .= "Имя: ".$_POST["firstname"]."\r\n";

if (isset($_POST["lastname"]) && !empty($_POST["lastname"]))
    $body .= "Фамилия: ".$_POST["lastname"]."\r\n";

if (isset($_POST["birth-date"]) && !empty($_POST["birth-date"]))
    $body .= "Дата рождения: ".$_POST["birth-date"]."\r\n";

if (isset($_POST["city"]) && !empty($_POST["city"]))
    $body .= "Город: ".$_POST["city"]."\r\n";

if (isset($_POST["ig-link"]) && !empty($_POST["ig-link"]))
    $body .= "Ссылка на инстаграм: ".$_POST["ig-link"]."\r\n";

if (isset($_POST["tel"]) && !empty($_POST["tel"]))
    $body .= "Телефон: ".$_POST["tel"]."\r\n";

if (isset($_POST["email"]) && !empty($_POST["email"]))
    $body .= "E-mail: ".$_POST["email"]."\r\n";

if (isset($_POST["expirience"]) && !empty($_POST["expirience"]))
    $body .= "Опыт в конкурсах красоты: ".$_POST["expirience"]."\r\n";

//mail("miss@molfarforum.com", $subject, $body);

$email->Subject   = $subject;
$email->Body      = $body;
//$email->AddAddress( 'imalex.m@ya.ru' );
//$email->AddAddress( 'miss@molfarforum.com' );
$email->AddAddress( 'inbox@molfarforum.com' );

if (isset($_FILES['images']) && !empty($_FILES['images']))
foreach($_FILES['images']['tmp_name'] AS $n => $path)
    $email->AddAttachment( $path , $_FILES['images']['name'][$n] );

$email->Send();

$result = array('success' => true);
echo json_encode($result);
