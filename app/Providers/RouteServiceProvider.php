<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    private function baseDomain(string $subdomain = ''): string
    {
        return (strlen($subdomain) > 0 ? "{$subdomain}.":'') . config('app.base_domain');
    }

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapNewsRoutes();

        $this->mapWebinarRoutes();

        $this->mapEventRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::domain($this->baseDomain())
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    protected function mapNewsRoutes()
    {
        Route::domain($this->baseDomain('news'))
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/news.php'));
    }

    protected function mapWebinarRoutes()
    {
        Route::domain($this->baseDomain('online'))
            ->name('online.')
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/online.php'));
    }

    protected function mapEventRoutes()
    {
        Route::domain($this->baseDomain('{year}'))
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/event.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::domain($this->baseDomain('api'))
             ->name('api.')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
