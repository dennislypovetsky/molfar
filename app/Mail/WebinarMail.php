<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\WebinarMail as WebinarMailModel;
use App\Webinar;
use App\WebinarParticipant;

class WebinarMail extends Mailable
{
    use Queueable, SerializesModels;

    const VAR_TIME_GREETINGS = '{{ timeGreetings }}';
    const VAR_REFERENCE = '{{ reference }}';
    const VAR_FIRSTNAME = '{{ firstname }}';
    const VAR_LASTNAME = '{{ lastname }}';
    const VAR_TOTAL_PRICE = '{{ totalPrice }}';
    const VAR_PROMOCODE = '{{ promocode }}';

    const VAR_WEBINAR_TITLE = '{{ webinarTitle }}';
    const VAR_WEBINAR_LECTOR = '{{ webinarLector }}';
    const VAR_WEBINAR_PRICE = '{{ webinarPrice }}';
    const VAR_WEBINAR_DATE = '{{ webinarDate }}';
    const VAR_WEBINAR_START_AT = '{{ webinarStartAt }}';
    const VAR_WEBINAR_END_AT = '{{ webinarEndAt }}';

    public static $variables = [
      self::VAR_TIME_GREETINGS,
      self::VAR_REFERENCE,
      self::VAR_FIRSTNAME,
      self::VAR_LASTNAME,
      self::VAR_TOTAL_PRICE,
      self::VAR_PROMOCODE,
      self::VAR_WEBINAR_TITLE,
      self::VAR_WEBINAR_LECTOR,
      self::VAR_WEBINAR_PRICE,
      self::VAR_WEBINAR_DATE,
      self::VAR_WEBINAR_START_AT,
      self::VAR_WEBINAR_END_AT,
    ];

    public $webinar;
    public $webinarMail;
    public $webinarParticipant;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Webinar $webinar, WebinarMailModel $webinarMail, WebinarParticipant $webinarParticipant)
    {
      $this->webinar = $webinar;
      $this->webinarMail = $webinarMail;
      $this->webinarParticipant = $webinarParticipant;
    }

    public static function varTitles()
    {
      return [
        self::VAR_TIME_GREETINGS => __('webinars.var_time_greetings'),
        self::VAR_REFERENCE => __('webinars.var_reference'),
        self::VAR_FIRSTNAME => __('webinars.var_firstname'),
        self::VAR_LASTNAME => __('webinars.var_lastname'),
        self::VAR_TOTAL_PRICE => __('webinars.var_total_price'),
        self::VAR_PROMOCODE =>  __('webinars.var_promocode'),

        self::VAR_WEBINAR_TITLE => __('webinars.var_webinar_title'),
        self::VAR_WEBINAR_LECTOR => __('webinars.var_webinar_lector'),
        self::VAR_WEBINAR_PRICE => __('webinars.var_webinar_price'),
        self::VAR_WEBINAR_DATE => __('webinars.var_webinar_date'),
        self::VAR_WEBINAR_START_AT => __('webinars.var_webinar_start_at'),
        self::VAR_WEBINAR_END_AT => __('webinars.var_webinar_end_at'),
      ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $webinar = $this->webinar;
      $webinarParticipant = $this->webinarParticipant;
      $webinarMail = $this->webinarMail;
      $subject = $webinarMail->subject;
      $bodyMD = $webinarMail->body;

      $varValues = [
        time_greetings(),
        $webinarParticipant->reference,
        $webinarParticipant->firstname,
        $webinarParticipant->lastname,
        price_uah($webinarParticipant->total_price),
        $webinarParticipant->promocode,
        $webinar->title,
        $webinar->person->name,
        price_uah($webinar->price),
        locale_date($webinar->date, '%d %s'),
        date('G:i', strtotime($webinar->start_at)),
        date('G:i', strtotime($webinar->end_at)),
      ];

      $subject = str_replace(
        self::$variables,
        $varValues,
        $subject
      );

      $bodyMD = str_replace(
        self::$variables,
        $varValues,
        $bodyMD
      );

      $webinarMailMD = new \App\Libs\WebinarMailMD();
      $webinarMailMD->setSafeMode(true);

      $bodyHTML = $webinarMailMD->text($bodyMD);

      return $this->subject($subject)->view('client-email.webinar-mail.webinar-mail',['body' => $bodyHTML]);
    }
}
