<?php

namespace App\Mail;

use Illuminate\Support\Facades\App;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Team;
use App\Ticket;
use App\Page;
use App\Poll;
use App\Card;

class NewTeam extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $team;
    public $poll;
    public $card;
    public $lang;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Team $team, Poll $poll, Card $card, $lang='ru')
    {
        $this->team = $team;
        $this->poll = $poll;
        $this->card = $card;
        $this->lang = $lang;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $team = $this->team;
      $poll = $this->poll;
      $card = $this->card;
      $lang = $this->lang;

      App::setLocale($lang);

      return $this->subject(__('emails.new-team_subject'))
        ->view('client-email.new-team.new-team', compact('team', 'card', 'poll'));
    }
}
