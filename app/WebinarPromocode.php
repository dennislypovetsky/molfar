<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebinarPromocode extends Model
{
  use SoftDeletes;

  protected $table = 'webinar_promocodes';

  protected $fillable = [
    'name',
    'code',
  ];

  protected $dates = [
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  public function groups()
  {
    return $this->belongsToMany('App\WebinarPromocodeGroup', 'webinar_promocode_groups_relations', 'promocode_id', 'group_id');
  }
}
