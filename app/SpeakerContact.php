<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpeakerContact extends Model
{
    protected $table = 'speakers_contacts';
    const TYPE_FACEBOOK = 'FACEBOOK';
    const TYPE_INSTAGRAM = 'INSTAGRAM';
    const TYPE_WEBSITE = 'WEBSITE';
    public static $types = [self::TYPE_FACEBOOK, self::TYPE_INSTAGRAM, self::TYPE_WEBSITE];

    public function speaker(){
        return $this->belongsTo('App\Speaker','speaker_id');
    }
}
