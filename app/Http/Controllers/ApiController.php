<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

use App\Event;

class ApiController extends Controller
{
    public function news(Request $request)
    {
//      if (!$request->ajax()) {return abort(404);}

      $result = [];
      $HTMLitems = [];

      $skip = $request->query('skip',0);
      $limit = \App\Post::LOAD_LIMIT;

      $items = \App\Post::published()->withTranslation()->latest()->skip($skip)->take($limit)->get();

      foreach($items as $item){
        $HTMLitems[] = view('web-site.news-item.news-item',['item' => $item])->render();
      }

      if(count($items) < $limit){
        $result['HTMLtail'] = view('web-site.news-tail.news-tail')->render();
      }

      $result['HTMLitems'] = $HTMLitems;

      return response()->json($result);
    }

    public function eventPartners(Request $request)
    {
//      if (!$request->ajax()) {return abort(404);}

      $skip = $request->query('skip',0);
      $event = Event::where('slug', $request->route('slug'))
      ->with(['partners' => function($query) use ($skip){
          $query->orderBy('name','asc')->withTranslation()->skip($skip)->take(1000);
      }])
      ->firstOrFail();

      return $this->responseJsonHtmlArray($event->partnerViewName, $event->partners);
    }

    public function eventJudges(Request $request)
    {
//      if (!$request->ajax()) {return abort(404);}

      $event = Event::where('slug', $request->route('slug'))
        ->with(['persons' => function($query){

          $query->commonJudges()->has('flow')->with(['flow','person' => function($query){
            $query->withTranslation();
          }])
          ->withTranslation()
          ->orderBy('order','asc');

        }])
        ->firstOrFail();

      return $this->responseJsonHtmlArray('web-site.dream-team__judge.dream-team__judge', $event->persons->sortBy('flow.order'));
    }

    public function pageTeamRegistration(Request $request)
    {
      $teamSlug = str_slug($request->name, '-');
      $request->merge(['slug' => $teamSlug]);

      $val = Validator::make($request->all(), [
        'name' => 'required|max:255',
        'slug' => 'required|unique:teams,slug',
        'contact_name' => 'required|max:255',
        'phone' => 'required|min:9|max:13|phone:AUTO,UA',
        'email'=> 'required|email',
        'city' => 'required|max:255'
      ],[
        'slug.unique' => __('teams.team_already_registered'),
      ]);

      if ($val->fails()) {
        return response()->json(['errors' => $val->messages()],422);
      }

      $page = \App\Page::findBySlug($request->route('slug'))
        ->has('event')
        ->has('poll')
        ->with([
          'event' => function($query){
            $query->with(['tickets' => function($query){
              $query
              ->with(['cards' => function($query){
                $query->withTranslation();
              }])
              ->orderBy('created_at');
            }]);
          },
          'poll' => function($query){
            $query->with(['children'=>function($query){
              $query->judge()->orderBy('begin_at');
            }]);
          }
        ])->firstOrFail();

        $ticket = $page->event->tickets->first();
//var_dump($page);
//exit;

        $card = $ticket->cards->first();
        $poll = $page->poll->children->first();

        $team = new \App\Team();

        $team->event_id = $page->event_id;

        $team->slug = $teamSlug;
        $team->name = $request->name;
        $team->contact_name = $request->contact_name;
        $team->phone = $request->phone;
        $team->email = $request->email;
        $team->city = $request->city;

        $team->save();

        event(new \App\Events\NewTeamEvent($team));

        Mail::to( $team->email )->send(new \App\Mail\NewTeam($team, $poll, $card, app()->getLocale()));

    	if ($request->ajax()) {
          return response()->json([
              'message'    =>  trans('messages.success'),
              'alert-type' => 'success',
              'type' => 'ajax'
          ]);
        }

       return back()->with('success', trans('messages.success'));
    }

    public function responseJsonHtmlArray(string $viewName, iterable $items)
    {
      $arrHtml = [];

      foreach($items as $item){
        $arrHtml[] = view($viewName, ['item' => $item])->render();
      }

      return response()->json(['html' => $arrHtml]);
    }
}
