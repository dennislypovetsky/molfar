<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Builder;

use View;
use App\Page;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
       View::share('INACTIVE_PAGES', Page::select('status', 'slug')->inactive()->pluck('slug')->toArray() );
       View::share('SRC_DIR', '/web-site');
       View::share('CACHE_VERSION', '0.6.22' );
       View::share('EVENT_YEAR', $this->getEventYear() );
       View::share('PREV_EVENT_YEAR', $this->getPrevEventYear() );
    }

    public function getEventYear(): string
    {
      return Route::input('year', setting('osnovnoe.subdomain'));
    }

  public function getPrevEventYear(): string
  {
    return setting('osnovnoe.prev_subdomain') ?: 2022;
  }

    public function addEventYearToSlug(string $slug = ''): string
    {
      return "{$this->getEventYear()}.$slug";
    }

    public function getPageSlug(): string
    {
      return Route::currentRouteName();
    }

    public function getPageBuilderBySlug(string $slug): Builder
    {
      return Page::findBySlug($slug)->withTranslation();
    }

    public function getPageBuilder()
    {
      return $this->getPageBuilderBySlug($this->getPageSlug());
    }

    public function getYearPageBuilder()
    {
      return $this->getPageBuilderBySlug(
        $this->addEventYearToSlug($this->getPageSlug())
      );
    }
}
