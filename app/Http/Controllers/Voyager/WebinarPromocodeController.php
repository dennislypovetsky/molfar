<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

use Validator;
use Illuminate\Validation\Rule;

class WebinarPromocodeController extends \TCG\Voyager\Http\Controllers\Controller
{
  public function addCode(Request $request)
  {
    $this->authorize('add', app('App\Webinar'));
    $requestData = $request->all();
    $this->validateAddCodeData($requestData)->validate();

    \App\WebinarPromocode::create($requestData);

    return response()->json([
      'message'    => __('voyager::generic.successfully_added_new'),
      'alert-type' => 'success',
    ]);
  }

  public function editGroup(Request $request, $id)
  {
    $this->authorize('edit', app('App\Webinar'));
    $dataTypeContent = \App\WebinarPromocodeGroup::findOrFail($id);
    return Voyager::view('voyager::webinar-promocodes.edit-add', compact('dataTypeContent'));
  }

  public function createGroup(Request $request)
  {
    $this->authorize('add', app('App\Webinar'));
    $dataTypeContent = new \App\WebinarPromocodeGroup;

    return Voyager::view('voyager::webinar-promocodes.edit-add', compact('dataTypeContent'));
  }

  public function updateGroup(Request $request, $id)
  {
    $this->authorize('edit', app('App\Webinar'));
    $requestData = $request->all();
    $this->validateGroupData($requestData, $id)->validate();

    $group = \App\WebinarPromocodeGroup::findOrFail($id);
    $group->fill($requestData);
    $group->save();
    $group->promocodes()->sync($requestData['promocodes'] ?? []);

    return redirect()
            ->route("voyager.webinar-promocodes.index")
            ->with([
                    'message'    => __('voyager::generic.successfully_updated'),
                    'alert-type' => 'success',
                ]);
  }

  public function storeGroup(Request $request)
  {
    $this->authorize('add', app('App\Webinar'));
    $requestData = $request->all();
    $this->validateGroupData($requestData)->validate();

    $group = \App\WebinarPromocodeGroup::create($requestData);
    $group->promocodes()->sync($requestData['promocodes'] ?? []);

    return redirect()
            ->route("voyager.webinar-promocodes.index")
            ->with([
                    'message'    => __('voyager::generic.successfully_added_new'),
                    'alert-type' => 'success',
                ]);
  }

  public static function dtFilter(string $table, array $request, array $relationFields = [])
  {
    $globalSearch = [];
    $columnSearch = [];
    $binding = [];
    $relationAliases = array_keys($relationFields);

    if ( isset($request['search']) && $request['search']['value'] != '' ) {
      $str = $request['search']['value'];

      for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
        $requestColumn = $request['columns'][$i];

        if ( $requestColumn['searchable'] == 'true' ) {
          $binding[] = "%$str%";
          $field = $requestColumn['data'];
          $globalSearch[] = (!in_array($field, $relationAliases) ? "$table.$field" : $relationFields[$field]).' LIKE ?';
        }
      }
    }

    // Individual column filtering
    if ( isset( $request['columns'] ) ) {
      for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
        $requestColumn = $request['columns'][$i];
        $str = $requestColumn['search']['value'];

        if ( $requestColumn['searchable'] == 'true' && $str != '' ) {
          $binding[] = "%$str%";
          $field = $requestColumn['data'];
          $columnSearch[] = (!in_array($field, $relationAliases) ? "$table.$field" : $relationFields[$field]).' LIKE ?';
        }
      }
    }

    // Combine the filters into a single string
		$where = '';

		if ( count( $globalSearch ) ) {
			$where = '('.implode(' OR ', $globalSearch).')';
		}

		if ( count( $columnSearch ) ) {
			$where = $where === '' ?
				implode(' AND ', $columnSearch) :
				$where .' AND '. implode(' AND ', $columnSearch);
    }

    return [$where, $binding];
  }

  public function index(Request $request)
  {
    $this->authorize('browse', app('App\Webinar'));

    return Voyager::view('voyager::webinar-promocodes.browse');
  }

  public function validateGroupData(array $fields, $id = null)
  {
    return Validator::make($fields, [
        'name' => ['required', 'max:255', Rule::unique('webinar_promocode_groups', 'name')->ignore($id)],
        'promocodes'=>'required|min:1',
        'promocodes.*' => 'exists:webinar_promocodes,id',
      ]);
  }

  public function validateAddCodeData(array $fields)
  {
    return Validator::make($fields, [
        'name' => 'required|max:255',
        'code' => 'required|regex:/^[a-z0-9\-]+$/|unique:webinar_promocodes,code|max:25',
      ]);
  }

  public static function dtOrder(&$builder, array $params)
  {
    if ( isset($params['order']) && count($params['order']) ) {
      for ( $i=0, $ien=count($params['order']) ; $i<$ien ; $i++ ) {
        // Convert the column index into the column data property
        $columnIdx = intval($params['order'][$i]['column']);
        $requestColumn = $params['columns'][$columnIdx];

        if ($requestColumn['orderable'] == 'true') {
          $builder->orderBy($requestColumn['data'], $params['order'][$i]['dir']);
        }
      }
    } else {
      $builder->orderBy('code', 'asc');
    }

    return $builder;
  }

  public function search(Request $request)
  {
    $this->authorize('browse', app('App\Webinar'));

    $params = $_GET;

    $relationFields = [
      'groupId' => 'webinar_promocode_groups.id',
      'groupName' => 'webinar_promocode_groups.name',
    ];

    $selectArr= ['webinar_promocodes.*'];

    foreach($relationFields as $alias => $field) {
      $selectArr[]= "$field as $alias";
    }

    $makeBuilder = function(array $selectArr=[]){
      return (\App\WebinarPromocode::selectRaw(implode(', ', $selectArr))
      ->leftJoin('webinar_promocode_groups_relations', 'webinar_promocode_groups_relations.promocode_id', '=', 'webinar_promocodes.id')
      ->leftJoin('webinar_promocode_groups', 'webinar_promocode_groups_relations.group_id', '=', 'webinar_promocode_groups.id'));
    };

    $builder = $makeBuilder($selectArr);

    $resTotalLength = $makeBuilder(['count(webinar_promocodes.id) as cnt'])->value('cnt');

    list($where, $binding) = self::dtFilter('webinar_promocodes', $params, $relationFields);

    if (!empty($where)) {
      $builder->whereRaw($where, $binding);
      $resFilterLength = $builder->count();
    } else {
      $resFilterLength = $resTotalLength;
    }

    $builder->orderBy($relationFields['groupName'], 'asc');

    self::dtOrder($builder, $params);

    $data = $builder
      ->take($params['length'] != -1 ? $params['length'] : 10)
      ->skip($params['start'] ?? 0)
      ->get();

    return response()->json(
      [
        "draw"            => isset ( $request['draw'] ) ?
          intval( $request['draw'] ) :
          0,
        "recordsTotal"    => intval( $resTotalLength ),
        "recordsFiltered" => intval( $resFilterLength ),
        "data"            => $data->toArray()
      ]
    );
  }

  public function relation(Request $request, string $type = 'promocode')
  {
    $typeModel = [
      'promocode' => \App\WebinarPromocode::class,
      'group' => \App\WebinarPromocodeGroup::class,
    ];
    $typeField = [
      'promocode' => 'code',
      'group' => 'name',
    ];
    $model = $typeModel[$type];
    $field = $typeField[$type];

    $page = $request->input('page');
    $on_page = 10;
    $search = $request->input('search', false);
    $skip = $on_page * ($page - 1);

    // If search query, use LIKE to filter results depending on field label
    if ($search) {
        $total_count = $model::where($field, 'LIKE', '%'.$search.'%')->count();
        $relationshipOptions = $model::take($on_page)->skip($skip)
            ->where('code', 'LIKE', '%'.$search.'%')
            ->get();
    } else {
        $total_count = $model::count();
        $relationshipOptions = $model::take($on_page)->skip($skip)->get();
    }

    $results = [];
    foreach ($relationshipOptions as $relationshipOption) {
        $results[] = [
            'id'   => $relationshipOption->id,
            'text' => $relationshipOption->{$field},
        ];
    }

    return response()->json([
        'results'    => $results,
        'pagination' => [
            'more' => ($total_count > ($skip + $on_page)),
        ],
    ]);
  }
}
