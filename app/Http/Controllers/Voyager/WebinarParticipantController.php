<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

use Validator;
use Illuminate\Validation\Rule;

class WebinarParticipantController extends \TCG\Voyager\Http\Controllers\Controller
{
  public static function dtFilter(string $table, array $request, array $relationFields = [])
  {
    $globalSearch = [];
    $columnSearch = [];
    $binding = [];
    $relationAliases = array_keys($relationFields);

    if ( isset($request['search']) && $request['search']['value'] != '' ) {
      $str = $request['search']['value'];

      for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
        $requestColumn = $request['columns'][$i];

        if ( $requestColumn['searchable'] == 'true' ) {
          $binding[] = "%$str%";
          $field = $requestColumn['data'];
          $globalSearch[] = (!in_array($field, $relationAliases) ? "$table.$field" : $relationFields[$field]).' LIKE ?';
        }
      }
    }

    // Individual column filtering
    if ( isset( $request['columns'] ) ) {
      for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
        $requestColumn = $request['columns'][$i];
        $str = $requestColumn['search']['value'];

        if ( $requestColumn['searchable'] == 'true' && $str != '' ) {
          $binding[] = "%$str%";
          $field = $requestColumn['data'];
          $columnSearch[] = (!in_array($field, $relationAliases) ? "$table.$field" : $relationFields[$field]).' LIKE ?';
        }
      }
    }

    // Combine the filters into a single string
		$where = '';

		if ( count( $globalSearch ) ) {
			$where = '('.implode(' OR ', $globalSearch).')';
		}

		if ( count( $columnSearch ) ) {
			$where = $where === '' ?
				implode(' AND ', $columnSearch) :
				$where .' AND '. implode(' AND ', $columnSearch);
    }

    return [$where, $binding];
  }

  public function index(Request $request)
  {
    $this->authorize('browse', app('App\Webinar'));

    return Voyager::view('voyager::webinar-participants.browse');
  }

  public function update(Request $request)
  {
    // Check permission
    $this->authorize('edit', app('App\Webinar'));

    $requestData = $request->all();
    $this->validateUpdateData($requestData)->validate();

    \App\WebinarParticipant::where('id', $requestData['id'])
    ->update($requestData);

    return response()->json([
      'message'    => __('voyager::generic.successfully_updated'),
      'alert-type' => 'success',
    ]);
  }

  public function validateUpdateData(array $fields)
  {
    return Validator::make($fields, [
        'id'               => 'required|exists:webinar_participants,id',
        'status'           => ['bail', 'sometimes', 'required', Rule::in(\App\WebinarParticipant::$statuses)],
        'is_made_call'     => 'bail|sometimes|required|boolean',
        'is_answered_call' => 'bail|sometimes|required|boolean',
      ]);
  }

  public static function dtOrder(&$builder, array $params)
  {
    if ( isset($params['order']) && count($params['order']) ) {
      for ( $i=0, $ien=count($params['order']) ; $i<$ien ; $i++ ) {
        // Convert the column index into the column data property
        $columnIdx = intval($params['order'][$i]['column']);
        $requestColumn = $params['columns'][$columnIdx];

        if ($requestColumn['orderable'] == 'true') {
          $builder->orderBy($requestColumn['data'], $params['order'][$i]['dir']);
        }
      }
    } else {
      $builder->orderByRaw('FIELD(webinar_participants.status, "'.implode('","',\App\WebinarParticipant::$statuses).'"), created_at DESC');
    }

    return $builder;
  }

  public function search(Request $request)
  {
    $this->authorize('browse', app('App\Webinar'));

    $params = $_GET;

    $relationFields = [
      'webinarTitle' => 'webinars.title',
      'webinarPersonName' => 'persons.name'
    ];

    $selectArr= ['webinar_participants.*'];

    foreach($relationFields as $alias => $field) {
      $selectArr[]= "$field as $alias";
    }

    $builder = \App\WebinarParticipant::selectRaw(implode(', ', $selectArr))
      ->leftJoin('webinars', 'webinar_participants.webinar_id', '=', 'webinars.id')
      ->leftJoin('persons', 'webinars.person_id', '=', 'persons.id');

    $resTotalLength = \App\WebinarParticipant::count();

    list($where, $binding) = self::dtFilter('webinar_participants', $params, $relationFields);

    if (!empty($where)) {
      $builder->whereRaw($where, $binding);
      $resFilterLength = $builder->count();
    } else {
      $resFilterLength = $resTotalLength;
    }

    self::dtOrder($builder, $params);

    $data = $builder
      ->take($params['length'] != -1 ? $params['length'] : 10)
      ->skip($params['start'] ?? 0)
      ->get();

    return response()->json(
      [
        "draw"            => isset ( $request['draw'] ) ?
          intval( $request['draw'] ) :
          0,
        "recordsTotal"    => intval( $resTotalLength ),
        "recordsFiltered" => intval( $resFilterLength ),
        "data"            => $data->toArray()
      ]
    );
  }

}
