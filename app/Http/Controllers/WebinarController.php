<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Webinar;

class WebinarController extends Controller
{
    public function webinars(Request $request)
    {
        $page = $this->getPageBuilder()->firstOrFail();
        $limit = Webinar::LOAD_LIMIT;
        $items = Webinar::published()->withPerson()->withTranslation()->latest()->take($limit)->get();
        return view('web-site.webinar', compact('page', 'items', 'limit'));
    }
}
