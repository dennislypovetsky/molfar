<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebinarParticipant extends Model
{
  use SoftDeletes;

  protected $fillable = [
    'webinar_id',
    'reference',
    'total_price',
    'firstname',
    'lastname',
    'email',
    'phone',
    'promocode',
    'city',
    'comment',
  ];

  protected $table = 'webinar_participants';

  const STATUS_NEW = 'NEW'; // recently received participant request
  const STATUS_APPROVED = 'APPROVED';
  const STATUS_PAID = 'PAID'; // fully paid
  const STATUS_CANCELED = 'CANCELED'; // participant request canceled

  public static $statuses = [
    self::STATUS_NEW,
    self::STATUS_APPROVED,
    self::STATUS_PAID,
    self::STATUS_CANCELED,
  ];

  protected $dates = [
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  protected $hidden = ['webinar_id'];

  public function afterSubmit()
  {
    return $this->hasMany('App\WebinarAfterSubmit', 'webinar_id', 'webinar_id');
  }

  public static function boot()
  {
      parent::boot();

      self::creating(function ($model) {
          $model->status = $model::STATUS_NEW;
      });

      self::updating(function ($model) {
        if ($model->status == $model->getOriginal('status')) {
          return;
        }
        if($model->status == WebinarParticipant::STATUS_PAID) {
            $model->payment_at = $model->freshTimestamp();
        }
      });
  }

  public function getPromocodeGroupIdsAttribute(): array
  {
    return isset($this->promocodeRelation, $this->promocodeRelation->groups) ? $this->promocodeRelation->groups->pluck('id')->toArray() : [];
  }

  public function promocodeRelation()
  {
    return $this->belongsTo('App\WebinarPromocode', 'promocode', 'code');
  }

  public function webinar()
  {
    return $this->belongsTo('App\Webinar', 'webinar_id');
  }
}
