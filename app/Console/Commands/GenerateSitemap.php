<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

use App\Post;
use App\Page;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $startYear = '2019';
      $currentYear = setting('osnovnoe.subdomain');
      $yearDiff = (int)$currentYear - (int)$startYear;
      $years = [$startYear];

      if($yearDiff > 0){
        for($i = 1; $i <= $yearDiff; $i++){
          $years[] = (string)((int)$startYear + $i); 
        }
      }
      $activePages = Page::select('status', 'slug')->active()->pluck('slug')->toArray();

      $sitemap = Sitemap::create();

      foreach([
        'main'             => [0.8, Url::CHANGE_FREQUENCY_MONTHLY],
        'dream-team'       => [0.8, Url::CHANGE_FREQUENCY_MONTHLY],
        'dream-team.rules' => [0.5, Url::CHANGE_FREQUENCY_MONTHLY],
        'recreation'       => [0.8, Url::CHANGE_FREQUENCY_MONTHLY],
        'throwback'        => [0.8, Url::CHANGE_FREQUENCY_MONTHLY],
        'schedule'         => [0.8, Url::CHANGE_FREQUENCY_MONTHLY],
      ] as $slug => $sitemapParams) {
        foreach($years as $year){
          $yearSlug = "$year.$slug";
          if(in_array($yearSlug, $activePages)){
            $sitemap->add(
              Url::create(
                route($slug, ['year' => $year])
                )->setPriority($sitemapParams[0])->setChangeFrequency($sitemapParams[1]));
          }
        }
      }
      if(in_array('privacy', $activePages)){
        $sitemap->add(Url::create(route('privacy'))->setPriority(0.5)->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY));
      }
      
      if(in_array('news', $activePages)){
        $sitemap->add(Url::create(route('news'))->setPriority(0.8)->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY));
      }
      $news = Post::published()->latest()->take(300)->get();

      $news->each(function($post) use ($sitemap){
        $sitemap->add(Url::create(route('article',['slug'=>$post->slug]))->setPriority(0.8)->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY));
      });

      $sitemap->writeToFile(public_path('sitemap.xml'));

    }
}
