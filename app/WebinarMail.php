<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebinarMail extends Model
{
  protected $table = 'webinar_mails';

  protected $fillable = [
    'webinar_id',
    'name',
    'subject',
    'body',
  ];

  public function webinar()
  {
      return $this->belongsTo('App\Webinar', 'webinar_id');
  }
}
