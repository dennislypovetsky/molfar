<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Traits\Translatable;

use App\Libs\WebinarMD;

class Webinar extends Model
{

  use SoftDeletes, Translatable {
    prepareTranslations as protected traitPrepareTranslations;
  }

   protected $table = 'webinars';
   protected $translatable = ['title', 'md', 'html','seo_title', 'meta_description'];

   protected $dates = [
    'created_at',
    'updated_at',
    'deleted_at'
  ];

   const STATUS_PUBLISHED = 'PUBLISHED';
   const STATUS_DRAFT = 'DRAFT';
   const STATUS_PENDING = 'PENDING';

   public static $statuses = [
    self::STATUS_PUBLISHED,
    self::STATUS_DRAFT,
    self::STATUS_PENDING,
  ];

  const LOAD_LIMIT = 5;

  public static function boot()
  {
    parent::boot();

    self::saving(function($model) {
      $model->updated_by = Auth::id();
    });

    self::creating(function($model) {
      $model->created_by = Auth::id();
    });
  }

  public function person()
  {
    return $this->belongsTo('App\Person','person_id');
  }

  public function save(array $options = [])
  {
      // Parse MD to HTML
      if($this->md){
        $this->html = $this->MDtoHTML($this->md);
      }
      parent::save($options);
  }

  public function MDtoHTML(string $md): string
  {
    $webinarMD = new WebinarMD();
    $webinarMD->setSafeMode(true);
    return $webinarMD->text($md);
  }

  public function getTitleWithPersonNameAttribute()
  {
    return (
      $this->person_id ? "{$this->person->name} — {$this->title}" : $this->title
    );
  }

  public function getTitleBrowseAttribute()
  {
    return $this->titleWithPersonName;
  }

  public function getImageBrowseAttribute()
  {
    return getThumbnail($this->image, 300);
  }

  public function getDateBrowseAttribute()
  {
    return (
      date('j.m.y', strtotime($this->date)). PHP_EOL.
      (
        $this->start_at ? ' '.date('G:i', strtotime($this->start_at)).
        ($this->end_at ? ' – '.date('G:i', strtotime($this->end_at)): '') : ''
      )
    );
  }

  public function scopeVoyager($query)
  {
      return $query->withPerson();
  }

  public function scopeWithPerson($query)
  {
      return $query->with('person:id,name');
  }

  public function scopePublished($query)
  {
      return $query->where('status', '=', static::STATUS_PUBLISHED);
  }

  public function prepareTranslations(&$request)
  {
    $mdTrans = null;
    $translations = [];

    // Translatable Fields
    $transFields = $this->getTranslatableAttributes();

    foreach ($transFields as $field) {
      if($field == 'html') continue;
      $trans = json_decode($request->input($field.'_i18n'), true);

      if($field == 'md'){
        $mdTrans = $trans;
      }

      // Set the default local value
      $request->merge([$field => $trans[config('voyager.multilingual.default', 'en')]]);

      $translations[$field] = $this->setAttributeTranslations(
          $field,
          $trans
      );

      // Remove field hidden input
      unset($request[$field.'_i18n']);
    }

    if(isset($mdTrans)) {
      $htmlTrans = [];

      foreach($mdTrans as $lang => $md){
        $htmlTrans[$lang] = $this->MDtoHTML($md);
      }

      // Set the default local value
      $request->merge(['html' => $trans[config('voyager.multilingual.default', 'en')]]);

      $translations['html'] = $this->setAttributeTranslations(
          'html',
          $htmlTrans
      );
    }

    // Remove language selector input
    unset($request['i18n_selector']);

    return $translations;
  }

}
