<?php

namespace App\Libs;

use Parsedown;

class WebinarMailMD extends Parsedown
{
    private $styleHeader = 'Margin:0;Margin-bottom:35px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:24px;font-weight:400;line-height:1.333;margin:0;margin-bottom:35px;padding:0;text-align:left;word-wrap:normal';
    private $styleParagraph = 'Margin:0;Margin-bottom:35px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.125;margin:0;margin-bottom:35px;padding:0;text-align:left';

    protected function blockHeader($Line)
    {
      $header = parent::blockHeader($Line);

        if (!isset($header)){
            return null;
        }
        $header['element']['name'] = 'h1';

        $header['element']['attributes']['style'] = $this->styleHeader;

        return $header;
    }

    protected function blockQuote($Line)
    {
      $quote = parent::blockQuote($Line);

        if (!isset($quote)){
            return null;
        }

        $quote['element']['attributes']['style'] = $this->styleParagraph;

        return $quote;
    }

    protected function paragraph($Line)
    {
      $paragraph = parent::paragraph($Line);

      if (!isset($paragraph)) {
          return null;
      }

      $paragraph['element']['attributes']['stye'] = $this->styleParagraph;

      return $paragraph;
    }
}
