<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebinarPromocodeGroup extends Model
{
  use SoftDeletes;

  protected $table = 'webinar_promocode_groups';

  protected $dates = [
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  protected $fillable = [
    'name'
  ];

  public function promocodes()
  {
    return $this->belongsToMany('App\WebinarPromocode', 'webinar_promocode_groups_relations', 'group_id', 'promocode_id');
  }
}
