<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebinarAfterSubmit extends Model
{
  protected $table = 'webinar_after_submits';

  const TYPE_PERCENT = 'PERCENT';
  const TYPE_FLAT = 'FLAT';
  const TYPE_FIXED = 'FIXED';

  public static $discountTypes = [self::TYPE_PERCENT, self::TYPE_FLAT, self::TYPE_FIXED];

  protected $fillable = [
    'webinar_id',
    'webinar_mail_id',
    'promocode_group_id',
    'discount_type',
    'discount_amount',
  ];

  public static function getDiscountTypeOptions(): array
  {
    return [
      self::TYPE_PERCENT => __('discount.type_percent'),
      self::TYPE_FLAT => __('discount.type_flat'),
      self::TYPE_FIXED => __('discount.type_fixed'),
    ];
  }

  public function promocodeGroup()
  {
      return $this->belongsTo('App\WebinarPromocodeGroup', 'promocode_group_id');
  }

  public function getDiscountTypeTitleAttribute()
  {
      return (self::getDiscountTypeOptions())[$this->discount_type] ?? 'None';
  }

  public function webinar()
  {
      return $this->belongsTo('App\Webinar', 'webinar_id');
  }

  public function webinarMail()
  {
      return $this->belongsTo('App\WebinarMail', 'webinar_mail_id');
  }
}
