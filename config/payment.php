<?php
  return [
    'account' => env('PAYMENT_ACCOUNT', 'account'),
    'secret' => env('PAYMENT_SECRET', 'secret'),
  ];
