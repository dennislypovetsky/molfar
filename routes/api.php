<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::name('events.')->prefix('events')->group(function () {
    Route::get('{slug}/partners','ApiController@eventPartners')->name('partners');
    Route::get('{slug}/judges','ApiController@eventJudges')->name('judges');
});

Route::get('/news','ApiController@news')->name('news');

Route::post('/pages/{slug}/team-registration','ApiController@pageTeamRegistration')->name('pages.team-registration');


