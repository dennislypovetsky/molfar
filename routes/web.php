<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Mail;

Route::get('/privacy','PageController@privacy')->name('privacy');


//Route::get('brow-pro',function() { return 'test';});

Route::get('/news', function(){
  return redirect()->route('news');
});
Route::get('/news/{slug}', function($slug){
  return redirect()->route('article', ['slug'=>$slug]);
});

Route::get('{page?}', function($page = ''){
  // return Redirect::to("//2020.".config('app.base_domain')."/$page");
  return redirect('https://'.setting('osnovnoe.subdomain').'.'.config('app.base_domain').'/'.$page);
})->where('page', 
  '(|throwback|schedule|recreation|dream\-team|buying|installment|brow\-pro|miss\-molfar|golden\-hands|foot\-profi)'
);


// Route::get('vote-broadcast', function(){
//   broadcast(new \App\Events\NewOnlineVoteEvent);
// });

// develpment routes

// Route::get('note', 'OrderController@notification');


// Route::get('/pdf-ticket', function () {
//     $lang = request()->query('lang', 'ru');

//     $order = \App\Order::inRandomOrder()->first();
//     $order->lang = $lang;

//     // $order -> options()->accommodation()->first()

//     // PDF::setOptions(['dpi' => 1, 'defaultFont' => 'dejavu sans condensed']);
//     // $data=[];
//     // $pdf = PDF::loadView('pdf.ticket', ['order'=>$order])->setPaper('a5', 'landscape');
//     // return $pdf->stream('download.pdf');
//     // return $pdf->download('order.pdf');



//     // PDF::loadHTML($html)->setPaper('a4')->setOrientation('portret')->setOption('margin-bottom', 0)->save('myfile.pdf');


//     app()->setlocale($lang);

//     $order->load(['ticket'=>function($query) use ($lang){
//       $query->withTranslation($lang);
//     }, 'card', 'bill_options'=>function($query) use ($lang){
//       $query->accommodation()->withTranslation($lang);
//     }]);

//     $ticket = $order->ticket;

//     $ticket->load('event');

//     $event = $ticket->event;

//     $card = $order->card;

//     $accommodation = isset( $order->bill_options[0]) ? $order->bill_options[0]->getTranslatedAttribute('name') : '—';

//     $pdf = PDF::loadView('pdf.ticket', compact('order','ticket','event','card','accommodation'))
//       ->setPaper('a4')
//       ->setOption('allow', [ public_path('/static/img/pdf/molfar.png') ]);
//       // ->setOrientation('portret')
//       // ->setOption('margin-left', 0)
//       // ->setOption('margin-right', 0)

//       return $pdf->stream();

// // return $pdf->download('ticket.pdf');


// });

// Route::get('mail/{type}',function($type){

//   if($type == 'dt'){
//     $team = \App\Team::inRandomOrder()->first();

//     return new App\Mail\NewTeam($team, request()->query('lang', 'ru'));
//   }

//   $order=App\Order::where('payment_type', $type)->inRandomOrder()->first();
//   $order->lang = request()->query('lang', 'ru');
//   // $order=App\Order::findOrFail(12);

//   Mail::to( $order->email )->send(new \App\Mail\NewOrder($order));

//   return new App\Mail\NewOrder($order);
// })->where('type', '(cash|installments|dt)');


Route::group(['prefix' => 'home'], function () {
  App::setLocale('ru');
  Voyager::routes();
});
