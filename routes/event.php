<?php
  Route::get('/','PageController@main')->name('main');
  Route::get('/throwback','PageController@throwback')->name('throwback');
  Route::get('/schedule','PageController@schedule')->name('schedule');
  Route::get('/recreation','PageController@recreation')->name('recreation');

  Route::get('/brow-pro','PageController@browPro')->name('brow-pro');
  Route::get('/miss-molfar','PageController@missMolfar')->name('miss-molfar');
  Route::get('/golden-hands','PageController@goldenHands')->name('golden-hands');
  Route::get('/foot-profi','PageController@footProfi')->name('foot-profi');

  Route::get('/dream-team','PageController@dreamTeam')->name('dream-team');
  Route::get('/dream-team/rules','PageController@dtRules')->name('dream-team.rules');
  Route::get('/dream-team/vote', 'VoteController@pageVote')->name('dream-team.vote');
  Route::get('/dream-team/vote/teams', 'VoteController@teams')->name('dream-team.vote-teams');
  Route::get('/dream-team/vote/results/{teamSlug}', 'VoteController@pageVoteResults')->name('dream-team.vote-results');

  Route::post('/dream-team/vote', 'VoteController@giveVote')->name('dream-team.give-vote');
  Route::post('/dream-team','PageController@dtRegistration')->name('dtRegistration');

  Route::get('language/{lang}', 'PageController@language')->name('langroute');

  Route::get('{payment_type}', 'OrderController@orderingProtectFromGet')->where('payment_type', '(buying|installment)');

  Route::post('{payment_type}', 'OrderController@ordering')->where('payment_type', '(buying|installment)');
  Route::post('checkout', 'OrderController@checkout')->name('checkout');
  Route::post('callback', 'PageController@callback')->name('callback');
