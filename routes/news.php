<?php
    Route::get('/','NewsController@news')->name('news');
    Route::get('/{slug}','NewsController@article')->name('article');
