<section class="last-year-winner lazyload">
  <div class="last-year-winner__container container">
    <h3 class="last-year-winner__title-wrapper">
      <span class="last-year-winner__description">@lang('dream-team2020-page.last-year-winner__description')</span>
      <strong class="last-year-winner__title">Winners</strong>
      <span class="last-year-winner__text">@lang('dream-team2020-page.last-year-winner__text')</span>
    </h3>
  </div>
</section>
