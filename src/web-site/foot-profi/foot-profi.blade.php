        <section class="foot-profi-hero">
          <div class="foot-profi-hero__container container">
            <header class="heading foot-profi-hero__heading">
              <h1 class="heading__title">Foot Profi Awards 2020
              </h1>
              <p class="heading__text">чемпионат по аппаратному педикюру
              </p><a class="button" href="#registration">Регистрация</a>
            </header>
          </div>
          <div class="foot-profi-hero__img-wrapper">
          </div>
        </section>
        <section class="org">
          <div class="org__container container">
            <div class="org__wrapper"><img class="org__img" src="{{$SRC_DIR}}/img/org/img/космотрейд.svg" width="437" alt="Логотип Космотрейд Бьюти Хаб" title=""/>
            </div>
            <div class="org__wrapper">
              <p class="org__text"><b>Организатор Foot Profi Awards</b> — институт подологии «Космотрейд»
              </p>
            </div>
          </div>
        </section>
        <section class="motivation">
          <div class="motivation__container container">
            <header class="heading motivation__heading">
              <h2 class="heading__title">Участие
              </h2>
            </header>
            <div class="motivation__wrapper">
              <div class="motivation__inner rellax" data-rellax-speed="-0.25">
                <p class="motivation__text">Заявки подаются до 1 сентября 2020 года
                </p>
              </div>
              <div class="motivation__inner rellax" data-rellax-speed="-0.5">
                <p class="motivation__text motivation__text--title">Участие в 3 шага
                </p>
                <ol class="motivation__list">
                  <li class="motivation__text-wrapper">
                    <p class="motivation__text">Заполнить анкету
                    </p>
                  </li>
                  <li class="motivation__text-wrapper">
                    <p class="motivation__text">Внести 100% оплату
                    </p>
                  </li>
                  <li class="motivation__text-wrapper">
                    <p class="motivation__text">Ознакомиться с <a class="motivation__link" href="https://drive.google.com/open?id=10o_1B8Lhk6TFKtRfolrl0_tNRV3keZmG&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">правилами</a> и <a class="motivation__link" href="https://drive.google.com/open?id=10pB5coci4F0wPOlftpMb_EC5gPhzpfNR&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">условиями</a>
                    </p>
                  </li>
                </ol>
              </div>
            </div>
          </div>
          <div class="motivation__container container container--wider">
            <div class="motivation__img-wrapper">
            </div>
          </div>
        </section>
        <section class="nominations">
          <div class="nominations__container container">
            <div class="nominations__wrapper">
              <header class="heading">
                <h2 class="heading__title">Номинации
                </h2>
              </header>
              <p class="nominations__text">Денежные сертификаты можно потратить в «Космотрейде»
              </p>
              <p class="nominations__text"><b>Юниор</b> — стаж практической деятельности менее 2-х лет
              </p>
              <p class="nominations__text"><b>Практик</b> — стаж практической деятельности от 2-х до 5-ти лет
              </p>
              <p class="nominations__text"><b>Профи</b> — стаж практической деятельности больше 5-ти лет
              </p>
            </div>
            <div class="nominations__wrapper swiper-container">
              <div class="swiper-wrapper">
                <div class="nominations-card swiper-slide">
                  <div class="nominations-card__wrapper nominations-card__wrapper--heading">
                    <div class="nominations-card__heading">
                      <h3 class="nominations-card__title nominations-card__title--h3">Юниор
                      </h3>
                    </div>
                  </div>
                  <div class="nominations-card__wrapper nominations-card__wrapper--text">
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">I место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 100 евро и сертификат на любое обучение в институте подологии Космотрейд
                      </p>
                    </div>
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">II место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 100 евро и сертификат на онлайн курс обучения
                      </p>
                    </div>
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">III место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 50 евро и сертификат на онлайн вебинары
                      </p>
                    </div>
                  </div>
                </div>
                <div class="nominations-card swiper-slide">
                  <div class="nominations-card__wrapper nominations-card__wrapper--heading">
                    <div class="nominations-card__heading">
                      <h3 class="nominations-card__title nominations-card__title--h3">Практик
                      </h3>
                    </div>
                  </div>
                  <div class="nominations-card__wrapper nominations-card__wrapper--text">
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">I место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 150 евро и сертификат на прохождение повышения квалификации + набор косметики
                      </p>
                    </div>
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">II место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 100 евро и сертификат на посещение конференции в институте подологии Космотрейд
                      </p>
                    </div>
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">III место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 75 евро и сертификат на онлайн курс обучения
                      </p>
                    </div>
                  </div>
                </div>
                <div class="nominations-card swiper-slide">
                  <div class="nominations-card__wrapper nominations-card__wrapper--heading">
                    <div class="nominations-card__heading">
                      <h3 class="nominations-card__title nominations-card__title--h3">Профи
                      </h3>
                    </div>
                  </div>
                  <div class="nominations-card__wrapper nominations-card__wrapper--text">
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">I место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 500 евро
                      </p>
                    </div>
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">II место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 150 евро и сертификат на прохождение повышения квалификации + набор косметики
                      </p>
                    </div>
                    <div class="nominations-card__inner">
                      <h4 class="nominations-card__title nominations-card__title--h4">III место
                      </h4>
                      <p class="nominations-card__text">Денежный сертификат номиналом 150 евро и сертификат на прохождение повышения квалификации + набор косметики
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>
        </section>
        <section class="registration" id="registration">
          <div class="registration__container container container--wider">
            <div class="registration__img-wrapper"><img class="registration__img" srcset="{{$SRC_DIR}}/img/foot-profi-registration/img/registration_720.jpg 360w, {{$SRC_DIR}}/img/foot-profi-registration/img/registration_1280.jpg 720w, {{$SRC_DIR}}/img/foot-profi-registration/img/registration_1600.jpg 1080w" src="{{$SRC_DIR}}/img/foot-profi-registration/img/registration_720.jpg" alt="" role="presentation"/>
            </div>
          </div>
          <div class="registration__container container">
            <header class="heading registration__heading">
              <h2 class="heading__title">Регистрация
              </h2>
            </header>
            <div class="registration__wrapper">
              <div class="registration-card registration-card--mf">
                <p class="registration-card__text registration-card__text--title">11 500 ₴
                </p>
                <div class="registration-card__wrapper">
                  <ul class="registration-card__list">
                    <li class="registration-card__text-wrapper">
                      <p class="registration-card__text">Участие в Чемпионате
                      </p>
                    </li>
                    <li class="registration-card__text-wrapper">
                      <p class="registration-card__text">Билет на Molfar Beauty Forum
                      </p>
                      <ul class="registration-card__list registration-card__list--inner">
                        <li class="registration-card__text-wrapper registration-card__text-wrapper--inner">
                          <p class="registration-card__text registration-card__text--inactive">жильё и завтраки
                          </p>
                        </li>
                        <li class="registration-card__text-wrapper registration-card__text-wrapper--inner">
                          <p class="registration-card__text registration-card__text--inactive">лекции и семинары
                          </p>
                        </li>
                        <li class="registration-card__text-wrapper registration-card__text-wrapper--inner">
                          <p class="registration-card__text registration-card__text--inactive">вечеринки с остальными участниками Форума
                          </p>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div><a class="button registration-card__button" href="https://foot-profi-awards-mf.paperform.co/" target="_blank" rel="noopener">Заполнить анкету</a>
              </div>
              <div class="registration-card registration-card--no-mf">
                <p class="registration-card__text registration-card__text--title">4 500 ₴
                </p>
                <div class="registration-card__wrapper">
                  <ul class="registration-card__list">
                    <li class="registration-card__text-wrapper">
                      <p class="registration-card__text">Участие в Чемпионате
                      </p>
                    </li>
                    <li class="registration-card__text-wrapper">
                      <p class="registration-card__text registration-card__text--inactive">Жильё и питание за свой счёт
                      </p>
                    </li>
                  </ul>
                </div><a class="button registration-card__button" href="https://foot-profi-awards.paperform.co/" target="_blank" rel="noopener">Заполнить анкету</a>
              </div>
            </div>
          </div>
        </section>
        <section class="contact">
          <div class="contact__container container">
            <header class="heading contact__heading">
              <h2 class="heading__title">Контакты
              </h2>
            </header>
            <div class="contact__wrapper">
              <div class="contact__inner contact__inner--top">
                <p class="contact__text">Тел.: <a class="contact__link" href="tel: +38 050 368 84 59">+38 050 368 84 59</a>
                </p>
                <p class="contact__text">e-mail: <a class="contact__link" href="mailto: suda1@krasa.kiev.ua">suda1@krasa.kiev.ua</a>
                </p>
                <p class="contact__text">Место проведения: ТРК «Буковель»
                </p>
              </div>
              <div class="contact__inner contact__inner--bot">
                <p class="contact__text">Бьюти Хаб «Космотрейд»
                </p>
                <p class="contact__text">Адрес: г. Киев, ул. Юлиуса Фучика, 13
                </p>
              </div>
            </div>
          </div>
        </section>