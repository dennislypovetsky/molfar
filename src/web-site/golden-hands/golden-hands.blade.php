        <section class="hero">
          <div class="hero__container container">
            <header class="heading hero__heading">
              <h1 class="heading__title">Золотые руки мира<span class="heading__dot">.</span><br/> Molfar, Украина
              </h1>
              <p class="heading__text">Международный чемпионат мастеров маникюра в Буковеле
              </p><a class="button button--accent heading__button" href="#registration">Регистрация</a>
            </header>
          </div>
          <div class="copyrights">Фото Алёны Смирновой
          </div>
        </section>
        <section class="about">
          <div class="about__container container">
            <header class="heading about__heading">
              <h2 class="heading__title">Что это?
              </h2>
              <p class="heading__text">«Золотые руки мира» — международный чемпионат мастеров маникюра, учреждённый Еленой Шанской. Этап «Molfar, Украина» пройдёт в рамках Molfar Beauty Forum 29 сентября в Буковеле.
              </p>
              <p class="heading__text">111 номинаций, 555 призовых мест, Гран-При, 3 призовых места для команд, а так же Зачёт Мольфара — с призовым фондом в $2000.
              </p>
            </header>
            <video class="video about__video" playsinline="playsinline" autoplay="autoplay" preload="auto" loop="loop" muted="muted"><source class="video__source" src="https://molfarforum.com/storage/golden-hands/v_600_0.webm" type="video/webm" media="only screen and (max-device-width: 480px)"/><source class="video__source" src="https://molfarforum.com/storage/golden-hands/v_mobile.mp4" type="video/mp4" media="only screen and (max-device-width: 480px)"/><source class="video__source" src="https://molfarforum.com/storage/golden-hands/v_600_0.webm" type="video/webm"/><source class="video__source" src="https://molfarforum.com/storage/golden-hands/v_desktop.mp4" type="video/mp4"/>
            </video>
          </div>
        </section>
        <section class="org">
          <div class="org__container container">
            <header class="heading org__heading">
              <h2 class="heading__title">Организаторы
              </h2>
            </header>
            <div class="org__wrapper">
              <div class="card org__card">
                <h3 class="card__title">Елена <span>Шанская</span>
                </h3>
                <ul class="card__list"><li>
                  <p class="card__text">Бьюти-коуч, ментор
                  </p></li><li>
                  <p class="card__text">Учредитель чемпионата «Золотые руки мира»
                  </p></li>
                </ul>
                <div class="card__img-wrapper loading"><img class="card__img lazyload" src="{{$SRC_DIR}}/img/org/img/org1_720.jpg" alt="Елена Шанская" title=""/>
                </div>
              </div>
              <div class="card org__card">
                <h3 class="card__title">Ксения <span>Сакелари</span>
                </h3>
                <ul class="card__list"><li>
                  <p class="card__text">Судья международной категории
                  </p></li><li>
                  <p class="card__text">Основатель одноимённого учебного центра
                  </p></li><li>
                  <p class="card__text">Организатор чемпионата «Золотые руки мира. Molfar, Украина»
                  </p></li>
                </ul>
                <div class="card__img-wrapper lazyload"><img class="card__img loading" src="{{$SRC_DIR}}/img/org/img/org2_720.jpg" alt="Ксения Сакелари" title=""/>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="nominations">
          <div class="nominations__container container">
            <header class="heading nominations__heading">
              <h2 class="heading__title">Номинации
              </h2>
            </header>
            <div class="nominations__wrapper nominations__wrapper--molfar">
              <div class="heading nominations__heading nominations__heading--background">
                <h3 class="heading__title heading__title--background">Очные номинации
                </h3>
              </div>
              <ol class="nominations__inner nominations__inner--nominations">
                <li class="nominations__card">
                  <ul class="nominations__text-wrapper"><li>
                    <p class="nominations__text nominations__text--title-counter">Салонный маникюр
                    </p></li><li>
                    <p class="nominations__text">40 минут <span class="nominations__text nominations__text--inactive">(50 — если «Дебют»)</span>
                    </p></li><li>
                    <p class="nominations__text">Выполнение маникюра на одной руке
                    </p></li><li>
                    <p class="nominations__text">Покрытие — красным гель-лаком
                    </p></li><li>
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/file/d/1_KpzxxL4h-_Itvg6C0_ow5yGPqbDUzG4/view?usp=sharing" target="_blank" rel="noopener">Полные правила</a>
                    </p></li>
                  </ul>
                  <div class="nominations__img-wrapper loading"><img class="nominations__img lazyload" data-srcset="{{$SRC_DIR}}/img/nominations/img/Salon-manicure_720.jpg 360w, {{$SRC_DIR}}/img/nominations/img/Salon-manicure_1080.jpg 720w" data-src="{{$SRC_DIR}}/img/nominations/img/Salon-manicure_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Салонный маникюр" title=""/><span class="nominations__text nominations__text--alt">Салонный маникюр</span>
                  </div>
                </li>
                <li class="nominations__card">
                  <ul class="nominations__text-wrapper"><li>
                    <p class="nominations__text nominations__text--title-counter">Салонный дизайн ногтей
                    </p></li><li>
                    <p class="nominations__text">30 минут для всех категорий
                    </p></li><li>
                    <p class="nominations__text">Выполняется на 5 типсах
                    </p></li><li>
                    <p class="nominations__text">Дизайн выполняется по картинке
                    </p></li><li>
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1-krfn5EmIBTYzo-disZK2sbg1OmC7b57&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Полные правила</a>
                    </p></li>
                  </ul>
                  <div class="nominations__img-wrapper loading"><img class="nominations__img lazyload" data-srcset="{{$SRC_DIR}}/img/nominations/img/Salon-nail-design_720.jpg 360w, {{$SRC_DIR}}/img/nominations/img/Salon-nail-design_1080.jpg 720w" data-src="{{$SRC_DIR}}/img/nominations/img/Salon-nail-design_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Салонный дизайн ногтей" title=""/><span class="nominations__text nominations__text--alt">Салонный дизайн ногтей</span>
                  </div>
                </li>
                <li class="nominations__card">
                  <ul class="nominations__text-wrapper"><li>
                    <p class="nominations__text nominations__text--title-counter">Салонное моделирование ногтей (выкладной белый френч)
                    </p></li><li>
                    <p class="nominations__text">90 минут для всех категорий
                    </p></li><li>
                    <p class="nominations__text">Моделирование на одной руке
                    </p></li><li>
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1ZBlE8d6D4cXBb_Jqm8aC8K8z_xAkHrZh&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Полные правила</a>
                    </p></li>
                  </ul>
                  <div class="nominations__img-wrapper loading"><img class="nominations__img lazyload" data-srcset="{{$SRC_DIR}}/img/nominations/img/Salon-nail-modeling_720.jpg 360w, {{$SRC_DIR}}/img/nominations/img/Salon-nail-modeling_1080.jpg 720w" data-src="{{$SRC_DIR}}/img/nominations/img/Salon-nail-modeling_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Салонное моделирование ногтей" title=""/><span class="nominations__text nominations__text--alt">Салонное моделирование ногтей</span>
                  </div>
                </li>
                <li class="nominations__card">
                  <ul class="nominations__text-wrapper"><li>
                    <p class="nominations__text nominations__text--title-counter">Nail-Art
                    </p></li><li>
                    <p class="nominations__text">90 минут для всех категорий
                    </p></li><li>
                    <p class="nominations__text">Необходимо максимально приближённо к оригиналу воспроизвести репродукцию картины на типсах
                    </p></li><li>
                    <p class="nominations__text">Автор картины учавствует в судействе и подарит оригинал победителю
                    </p></li><li>
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1pO02gtUn1O1yOkGZskpgX7k_M48oB3zW&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Полные правила</a>
                    </p></li>
                  </ul>
                  <div class="nominations__img-wrapper loading"><img class="nominations__img lazyload" data-srcset="{{$SRC_DIR}}/img/nominations/img/Nail-Art_720.jpg 360w, {{$SRC_DIR}}/img/nominations/img/Nail-Art_1080.jpg 720w" data-src="{{$SRC_DIR}}/img/nominations/img/Nail-Art_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Nail-Art" title=""/><span class="nominations__text nominations__text--alt">Nail-Art</span>
                  </div>
                </li>
              </ol>
              <div class="nominations__inner nominations__inner--rules">
                <ul class="nominations__list nominations__list--rules"><li>
                  <p class="nominations__text nominations__text--title">Общие правила очного участия
                  </p></li><li>
                  <p class="nominations__text">Организатор предоставляет для работы стол, розетку и два стула
                  </p></li><li>
                  <p class="nominations__text">Мастер должен иметь свои материалы и инструменты, необходимые для выполнения работы: дезинфекторы, 2 лампы (осветительную и для работы с гелем), удлинитель
                  </p></li><li>
                  <p class="nominations__text">Модель предоставляет организатор. Стоимость модели €10 / номинация
                  </p></li><li>
                  <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1qaOhxFkKkRyzXk5ZJ-lAd7aYa_4VKg6E&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Подробные правила и регламент</a>
                  </p></li>
                </ul>
                <ul class="nominations__list nominations__list--rules"><li>
                  <p class="nominations__text nominations__text--title">Требования к внешнему виду мастера
                  </p></li><li>
                  <p class="nominations__text">Собранные волосы
                  </p></li><li>
                  <p class="nominations__text">Чёрный верх, чёрные брюки или юбка ниже колен офисного стиля
                  </p></li><li>
                  <p class="nominations__text">Сменная обувь с закрытым носком чёрного цвета
                  </p></li><li>
                  <p class="nominations__text">Отсутствие ярких аксессуаров
                  </p></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="nominations__wrapper nominations__wrapper--distance">
            <div class="nominations__container nominations__container--distance container">
              <div class="nominations__inner nominations__inner--distance-nominations">
                <div class="heading nominations__heading nominations__heading--background nominations__heading--left">
                  <h3 class="heading__title heading__title--background">Заочные номинации
                  </h3>
                </div>
                <ol class="nominations__list nominations__list--distance">
                  <li class="nominations__text-wrapper nominations__text-wrapper--distance">
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1HdCAbGJjL8ut02b5x1o3u33aYiH_FKBK&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Образ на модели (1 номинация)</a>
                    </p>
                  </li>
                  <li class="nominations__text-wrapper nominations__text-wrapper--distance">
                    <p class="nominations__text">Дизайн ногтей (41 номинация)
                      <ol class="nominations__list nominations__list--distance-inner">
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1yTbzFIJu6I-wYuHi0_VOGZXVfYDXvFA2&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Художественная роспись (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1-zOKFhPl_5N9QK_usnmHbJhb38cAcwNX&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Акварель (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1EWycMalNroV2gzt5OF_7R_ud1WobQ0G6&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Mix Media (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1mD4A_33c2pdVfs2eQca_koLIp2htNVVS&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Китайская роспись (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1s1CeLyrW2JaMV2y_j2PhLZM9EtUfUCN-&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Музеи Мира (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1_ZtlXAB2reTyC5-kF_2c0mC4OdUShYWX&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">3D дизайн (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1USzLoA0ejgVeV0cG5jqMvlQTcMLwrIOu&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Лепка плоскостная (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=17kv3uMU3doQO7CyBctzPygkYzB-h9SuZ&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Один в один — типсы (художественная роспись)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1qciLOvnU8qfIKPg2idJjUSzW2ao9oYnS&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Скульптура</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1RCbTJkdLCcg-YXKBrh5TMs5b5Yy5plwr&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Жостово</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1l_XdpRU8vgvFr2A60VYS2riRuqKNye8v&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Инкрустация (типсы)</a>
                          </p>
                        </li>
                      </ol>
                    </p>
                  </li>
                  <li class="nominations__text-wrapper nominations__text-wrapper--distance">
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=12WGAojn2BPwdegAFVxdgUqcI235H4Taq&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Фристайл (10 номинаций)</a>
                    </p>
                  </li>
                  <li class="nominations__text-wrapper nominations__text-wrapper--distance">
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1Mtmovd8BtqmuhoHL56nHgO76caB8ugA_&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Аэрография (5 номинации)</a>
                    </p>
                  </li>
                  <li class="nominations__text-wrapper nominations__text-wrapper--distance">
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=15NpwP5Hqm2UOu9GCKy4MXh2727y5oa0r&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Ювелирные украшения (10 номинаций)</a>
                    </p>
                  </li>
                  <li class="nominations__text-wrapper nominations__text-wrapper--distance">
                    <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1E9W075jlsZvyaA1DU72sIB3Qpkn2_q7U&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Постер (18 номинаций)</a>
                    </p>
                  </li>
                  <li class="nominations__text-wrapper nominations__text-wrapper--distance">
                    <p class="nominations__text">Педикюрные типсы (22 номинаций)
                      <ol class="nominations__list nominations__list--distance-inner">
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1b7hQvu2eddq-vFuqWJuAQ9Aq2aP3Eicc&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Художественная роспись (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1LA0phIaygCkhouYb8I0AEgoPHYBdLNZe&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Акварель (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1J4OWyVROM0TH8wmGIwspiRHuYadj-Lkh&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Mix Media (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1Y-CpU-JdiEFSLZm9bQ1tb2rHCJmBBU20&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Китайская роспись (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1sCSM0jKwa0JOApYGD7huCvI5zmp1xpBF&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Лепка плоскостная (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1x0NV6wyKMCUzgp5Eg-Uu8i-DWC_lFJip&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Инкрустация (типсы)</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1dHTq8GRM8BcWIn_tOW10PxtDmwV2gDwR&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Жостово</a>
                          </p>
                        </li>
                        <li class="nominations__text-wrapper nominations__text-wrapper--distance-inner">
                          <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=13cm8K-jJYAJLxBknFNZCGm7ghTm1LB6F&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Аэрография (типсы)</a>
                          </p>
                        </li>
                      </ol>
                    </p>
                  </li>
                </ol>
              </div>
              <div class="nominations__inner nominations__inner--distance-rules">
                <ol class="nominations__list nominations__list--distance-rules"><li>
                  <p class="nominations__text nominations__text--title">Общие правила заочного участия
                  </p></li><li>
                  <p class="nominations__text">Заочные работы принимаются до 20 сентября
                  </p></li><li>
                  <p class="nominations__text">При заочном участии личное присутствие не обязательно
                  </p></li><li>
                  <p class="nominations__text">Победители будут озвучены в прямой трансляции и группе для участников
                  </p></li><li>
                  <p class="nominations__text"><a class="nominations__link" href="https://drive.google.com/open?id=1u9I5KAlqagxXN_ctTn0MlrnNBkwmgnGZ&amp;authuser=inbox%40molfarforum.com&amp;usp=drive_fs" target="_blank" rel="noopener">Подробные правила и регламент</a>
                  </p></li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section class="grading">
          <div class="grading__container container">
            <header class="heading grading__heading">
              <h2 class="heading__title">Оценивание
              </h2>
            </header>
            <div class="grading__wrapper">
              <ul class="grading__inner"><li>
                <p class="grading__text">Очные и заочные номинации судят отдельно
                </p></li><li>
                <p class="grading__text">Все номинации оцениваются в 5–10 критериях в зависимости от номинации
                </p></li>
              </ul>
              <ul class="grading__inner"><li>
                <p class="grading__text">Судейский комитет состоит из 5 судей
                </p></li><li>
                <p class="grading__text">1 судья — 1 критерий
                </p></li><li>
                <p class="grading__text">До начала соревнований статисты проводят процедуру Nail check, в которой отмечают наличие каких-либо повреждений на поверхности кожи и ногтей
                </p></li>
              </ul>
            </div>
          </div>
        </section>
        <div class="parallax">
          <div class="parallax__img-wrapper"><img class="parallax__img" src="{{$SRC_DIR}}/img/parallax/img/parallax_1440.jpg" alt="" role="presentation"/>
          </div>
        </div>
        <section class="prize">
          <div class="prize__container container">
            <header class="heading prize__heading">
              <h2 class="heading__title">Призы
              </h2>
            </header>
            <div class="prize__wrapper">
              <ul class="prize__list">
                <li class="prize__text-wrapper prize__text-wrapper--title">
                  <p class="prize__text prize__text--title">555 призовых мест в 111 номинациях
                  </p>
                </li>
                <li class="prize__text-wrapper">
                  <p class="prize__text">Участники, занявшие 1, 2 и 3 места, получают дипломы, медали и кубки
                  </p>
                </li>
                <li class="prize__text-wrapper">
                  <p class="prize__text">Участники, занявшие 4 и 5 места, получают дипломы
                  </p>
                </li>
                <li class="prize__text-wrapper">
                  <p class="prize__text">Каждому участнику присуждается «сертификат участника»
                  </p>
                </li>
                <li class="prize__text-wrapper">
                  <p class="prize__text">В каждой номинации 5 призовых мест
                  </p>
                </li>
                <li class="prize__text-wrapper">
                  <p class="prize__text">Участники, победившие в 3 номинациях и больше, соревнуются за кубок Гран-При
                  </p>
                </li>
              </ul>
              <div class="prize__inner">
                <div class="heading prize__heading prize__heading--background">
                  <h3 class="heading__title heading__title--background">Зачёт Мольфара
                  </h3>
                </div>
                <ul class="prize__list prize__list--inner">
                  <li class="prize__text-wrapper">
                    <p class="prize__text">Для очных участников, Molfar Forum разыгрывает дополнительный призовой фонд в $2000
                    </p>
                    <ul class="prize__list prize__list--inner"><li>
                      <p class="prize__text">$1000 — Гран-При Зачёта
                      </p></li><li>
                      <p class="prize__text">$500 — 1-е место
                      </p></li><li>
                      <p class="prize__text">$300 — 2-е место
                      </p></li><li>
                      <p class="prize__text">$200 — 3-е место
                      </p></li>
                    </ul>
                  </li>
                  <li class="prize__text-wrapper">
                    <p class="prize__text">В зачёт идут баллы, набранные за участие в 5 обязательных номинациях:
                    </p>
                    <ul class="prize__list prize__list--inner prize__list--dots"><li>
                      <p class="prize__text">3 очные
                      </p></li><li>
                      <p class="prize__text">2 заочные
                      </p></li>
                    </ul>
                  </li>
                  <li class="prize__text-wrapper">
                    <p class="prize__text">Номинации для зачёта участник выбирает во время регистрации на Molfar Forum
                    </p>
                  </li>
                </ul>
              </div>
            </div>
            <div class="prize__wrapper">
              <ul class="prize__list">
                <li class="prize__text-wrapper prize__text-wrapper--title">
                  <p class="prize__text prize__text--title">Командное участие
                  </p>
                </li>
                <li class="prize__text-wrapper">
                  <p class="prize__text">Команда это тренер и 5 мастеров
                  </p>
                </li>
                <li class="prize__text-wrapper">
                  <p class="prize__text">€2 750 — стоимость участия команды. В цену входят:
                  </p>
                  <ul class="prize__list prize__list--inner prize__list--dots"><li>
                    <p class="prize__text">Билеты на Molfar Forum для мастеров и тренера
                    </p></li><li>
                    <p class="prize__text">30 номинаций (15 очных и 15 заочных)
                    </p></li>
                  </ul>
                </li>
                <li class="prize__text-wrapper">
                  <p class="prize__text">Тренер может усилить команду дополнительными участниками. Стоимость каждого нового участника €550
                  </p>
                </li>
              </ul>
              <div class="prize__inner">
                <div class="heading prize__heading prize__heading--background">
                  <h3 class="heading__title heading__title--background">Командный зачёт
                  </h3>
                </div>
                <ul class="prize__list prize__list--inner"><li>
                  <p class="prize__text">3 призовых места для команд
                  </p></li><li>
                  <p class="prize__text">Зачёт складывается из суммы баллов, набранных командой
                  </p></li><li>
                  <p class="prize__text">В зачёт идут баллы, набранные за участие в 25 номинациях. Номинации для зачёта выбираются при регистрации на Molfar Forum
                  </p></li><li>
                  <p class="prize__text">5 лучших результатов в номинации идут в зачёт. При этом количество участников в номинации не ограниченно
                  </p></li><li>
                  <p class="prize__text">Мастера из команды участвуют в Зачёте Мольфара, сражаясь за призовой фонд
                  </p></li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section class="cost">
          <div class="cost__container container">
            <header class="heading cost__heading">
              <h2 class="heading__title">Стоимость участия
              </h2>
            </header>
            <div class="cost__wrapper">
              <div class="cost__inner cost__inner--wrapper">
                <div class="heading cost__heading cost__heading--background">
                  <h3 class="heading__title heading__title--background">Мастерам
                  </h3>
                </div>
                <div class="cost__inner">
                  <div class="cost__text-wrapper">
                    <p class="cost__text cost__text--title cost__text--dashed">До €75 за работу
                    </p>
                    <div class="tooltip">
                      <p class="tooltip__text">1 номинация — €100
                      </p>
                      <p class="tooltip__text">2 номинации — по  €90
                      </p>
                      <p class="tooltip__text">3 номинации — по €80
                      </p>
                      <p class="tooltip__text">4 номинации и больше — по €75
                      </p>
                    </div>
                  </div>
                  <p class="cost__text">За категорию «мастер», «мастер-инструктор», «VIP», «премиум», начиная с 4-й номинации
                  </p>
                </div>
                <div class="cost__inner">
                  <div class="cost__text-wrapper">
                    <p class="cost__text cost__text--title cost__text--dashed">До €55 за работу
                    </p>
                    <div class="tooltip">
                      <p class="tooltip__text">1 номинация — €70
                      </p>
                      <p class="tooltip__text">2 номинации — по €65
                      </p>
                      <p class="tooltip__text">3 номинации — по €60
                      </p>
                      <p class="tooltip__text">4 номинации и больше — по €55
                      </p>
                    </div>
                  </div>
                  <p class="cost__text">За категорию «дебют», начиная с 4-й номинации
                  </p>
                </div>
                <div class="cost__inner">
                  <p class="cost__text cost__text--faded">От 8 500 ₴
                  </p>
                  <p class="cost__text cost__text--title">От 7 500 ₴
                  </p>
                  <p class="cost__text">Обойдётся билет на Molfar Forum участникам чемпионата «Золотые руки мира. Molfar, Украна». Это ~€250
                  </p>
                  <p class="cost__text">В билет, кроме <a class="cost__link" href="https://2020.molfarforum.com/schedule" target="_blank" rel="noopener">лекций и семинаров</a>, входит жильё, завтраки, и оплаченные вечеринки с остальными участниками Форума
                  </p>
                </div>
                <div class="cost__inner cost__inner--line">
                  <p class="cost__text cost__text--title">Например:
                  </p>
                  <p class="cost__text">€625 участие в <i>5</i> номинациях
                  </p>
                </div>
              </div>
              <div class="cost__inner cost__inner--wrapper">
                <div class="heading cost__heading cost__heading--background">
                  <h3 class="heading__title heading__title--background">Командам
                  </h3>
                </div>
                <div class="cost__inner">
                  <p class="cost__text cost__text--title">€2 750
                  </p>
                  <p class="cost__text">Стоимость участия команды из 6 человек (1 тренер и 5 мастеров)
                  </p>
                  <p class="cost__text">Это €550 на человека или почти на €200 выгоднее, чем одиночное участие. Для тренера участие — бесплатно
                  </p>
                  <p class="cost__text">Команда получает 15 очных и 15 заочных номинаций включённых в цену. Дополнительные номинации €75 для «мастер»+ и €55 для «дебют»
                  </p>
                  <p class="cost__text">Командный пакет на «Золотые руки мира. Molfar, Украина» даёт право на участие во всех событиях Molfar Forum: шоу, розыграши, вечеринки. А также жильё, завтраки, лекции и семинары
                  </p>
                </div>
                <div class="cost__inner cost__inner--line">
                  <p class="cost__text cost__text--title">Или:
                  </p>
                  <p class="cost__text">€550 на члена команды
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="registration" id="registration">
          <div class="registration__container container">
            <header class="heading registration__heading">
              <h2 class="heading__title">Регистрация
              </h2>
              <p class="heading__text">Открыта до 20 сентября
              </p>
              <p class="heading__text heading__text--counter">Осталось 34 очных места
              </p>
            </header>
            <div class="registration__wrapper">
              <div class="ticket registration__ticket">
                <div class="ticket__wrapper">
                  <p class="ticket__text">Отправляйтесь на Molfar Forum, <span class="ticket__text ticket__text--faded">купив билет со скидкой</span>
                  </p>
                  <p class="ticket__text">Получите приглашение <span class="ticket__text ticket__text--faded">в закрытую группу участников Чемпионата</span>
                  </p>
                  <p class="ticket__text">Выбирайте номинации, <span class="ticket__text ticket__text--faded">а наш конструктор номинаций посчитает стоимость</span>
                  </p>
                </div>
                <div class="ticket__inner"><a class="button button--accent ticket__button" href="https://golden-hands.paperform.co/">Мастерам</a>
                </div>
              </div>
              <div class="ticket registration__ticket">
                <div class="ticket__wrapper">
                  <p class="ticket__text">Представьте свою школу <span class="ticket__text ticket__text--faded">на Чемпионате</span>
                  </p>
                  <p class="ticket__text">Соберите команду <span class="ticket__text ticket__text--faded">и получите в распоряжение сразу 30 номинаций</span>
                  </p>
                  <p class="ticket__text">Усильте команду <span class="ticket__text ticket__text--faded">дополнительными участниками или номинациями, а наш конструктор скажет стоимость</span>
                  </p>
                </div>
                <div class="ticket__inner"><a class="button button--accent ticket__button" href="https://golden-hands-team.paperform.co/">Командам</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="contacts">
          <div class="contacts__container container">
            <header class="heading contacts__heading">
              <h2 class="heading__title">Контакты
              </h2>
            </header>
            <div class="contacts__wrapper">
              <div class="contacts__inner contacts__inner--wrapper">
                <p class="contacts__text"><a class="contacts__link" href="tel:+380 (96) 367 10 10">+380 (96) 367 10 10</a>
                </p>
                <p class="contacts__text">Евгений Сакелари, координатор Чемпионата (сотовый, Вайбер, Телеграм)
                </p>
              </div>
              <div class="contacts__inner contacts__inner--wrapper">
                <div class="contacts__inner">
                  <p class="contacts__text"><a class="contacts__link" href="https://www.instagram.com/shanskaya_lena/" target="_blank" rel="noopener">@shanskaya_lena</a>
                  </p>
                  <p class="contacts__text">Инстаграм Елены Шанской
                  </p>
                </div>
                <div class="contacts__inner">
                  <p class="contacts__text"><a class="contacts__link" href="https://www.instagram.com/sakelary.nail/" target="_blank" rel="noopener">@sakelary.nail</a>
                  </p>
                  <p class="contacts__text">Инстаграм школы Ксении Сакелари
                  </p>
                </div>
                <div class="contacts__inner">
                  <p class="contacts__text"><a class="contacts__link" href="https://www.instagram.com/molfarforum/" target="_blank" rel="noopener">@molfarforum</a>
                  </p>
                  <p class="contacts__text">Инстаграм Molfar Beauty Forum
                  </p>
                </div>
              </div>
              <div class="contacts__inner contacts__inner--wrapper">
                <div class="heading contacts__heading contacts__heading--background">
                  <h3 class="heading__title heading__title--background">Заочные работы
                  </h3>
                </div>
                <div class="contacts__inner">
                  <p class="contacts__text contacts__text--title">Украинский адрес для заочных работ:
                  </p>
                  <p class="contacts__text">Одесса, Переулок Маланова, 6. Новая Почта: склад 129. На имя Евгения Сакелари<br/>+380 (96) 367 10 10
                  </p>
                  <p class="contacts__text contacts__text--inactive">Сообщите информацию о грузе получателю
                  </p>
                </div>
                <div class="contacts__inner">
                  <p class="contacts__text contacts__text--title">Российский адрес для заочных работ:
                  </p>
                  <p class="contacts__text">Москва, инд. 109651. Елена Шанская<br/>+7 (985) 774 11 64
                  </p>
                  <p class="contacts__text contacts__text--inactive">Пересылка работ только через курьерскую службу
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
