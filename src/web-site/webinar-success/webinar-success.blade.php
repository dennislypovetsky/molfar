<section class="webinar-success">
    <div class="webinar-success__container container">
      <header class="webinar-success__heading">
        <h1 class="title title--webinar-success webinar-success__title">Проверьте почту
        </h1>
        @if(isset($order))
        <p style="display: none">Order status: {{$order->getStatus()}}</p>
        @endif
        <p class="webinar-success__text">Вы успешно записались на вебинар. На указанную почту выслано письмо с инструкциями.
        </p><a class="button button--webinar-success" href="{{route('online.webinar',[
          'slug' => $item->slug
        ])}}"><span>Запишите друга</span></a>
      </header>
    </div>
</section>
