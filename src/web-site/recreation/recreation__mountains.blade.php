<section class="activities activities--mountains activities-parallax">
  <div class="activities__bg activities__bg--mountains lazyload"></div>
  <picture class="activities__img-wrapper activities-parallax__layer activities-parallax__layer--last loading" data-rellax-speed="4">
    <source type="image/webp" data-srcset="{{$SRC_DIR}}/img/recreation/mountain_1080.webp 1080w, {{$SRC_DIR}}/img/recreation/mountain_1440.webp 1440w, {{$SRC_DIR}}/img/recreation/mountain_1920.webp 1920w">
    <source type="image/png" data-srcset="{{$SRC_DIR}}/img/recreation/mountain_1080.png 1080w, {{$SRC_DIR}}/img/recreation/mountain_1440.png 1440w, {{$SRC_DIR}}/img/recreation/mountain_1920.png 1920w"><img class="activities__img lazyload" data-src="{{$SRC_DIR}}/img/recreation/mountain_1080.png">
  </picture>
  <div class="container activities__container">
    <div class="activities__wrapper">
      <h2 class="activities__title">@lang('recreation.activities__title--mountains')</h2>
      <p class="activities__description">@lang('recreation.activities__description--mountains')</p>
    </div>
  </div>
</section>
