@extends('web-site.layout-rules.layout-rules')

@section('title', $page->getTranslatedAttribute('seo_title'))

@section('description', $page->getTranslatedAttribute('meta_description'))

@section('ogImage', url($SRC_DIR.'/img/og-image--dream-team.jpg'))

@section('pageModifier', 'page--rules')

@section('keywords', $page->getTranslatedAttribute('meta_keywords'))

@section('content')
  @include('web-site.rules.rules')
@endsection

@push('scripts')
   <script defer src="{{$SRC_DIR}}/js/rules.min.js?v={{$CACHE_VERSION}}"></script>
@endpush
