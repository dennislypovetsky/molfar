<section class="hero hero--throwback">
  <div class="hero__container container">
    <div class="hero__title-wrapper">
      <h1 class="hero__title hero__title--throwback visually-hidden">Molfar Beauty Forum ‘19</h1>
      <strong class="hero__title hero__title--throwback">@lang('throwback2020.hero__title')</strong>
      <p class="hero__description hero__description--throwback">@lang('throwback2020.hero__description')</p>
    </div>
    <div class="hero__video-wrapper">
      <div class="hero__video loading">
        <iframe class="lazyload" data-src="https://www.youtube.com/embed/Kel9ADPbLbM?rel=0&amp;showinfo=0" width="560" height="315" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</section>
