@extends('web-site.layout.layout')

@section('title', 'Ошибка — Molfar Online')
@section('pageModifier', 'webinar--error')

@section('content')
  @include('web-site.webinar-error.webinar-error')
@endsection

@push('scripts')
  {{-- <script defer src="{{$SRC_DIR}}/js/webinar-success.min.js?v={{$CACHE_VERSION}}"></script> --}}
@endpush
