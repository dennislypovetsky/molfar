@extends('web-site.layout.layout')

@section('title', $page->getTranslatedAttribute('seo_title'))

@section('description', $page->getTranslatedAttribute('meta_description'))

@section('ogImage', url($SRC_DIR.'/img/og-image.jpg'))

@section('pageModifier', 'page--privacy')

@section('keywords', $page->getTranslatedAttribute('meta_keywords'))

@section('content')
  @include('web-site.privacy.privacy')
@endsection

@push('scripts')
  {{-- <script defer src="{{$SRC_DIR}}/js/privacy.min.js?v={{$CACHE_VERSION}}"></script> --}}
@endpush
