<link rel="icon" type="image/png" sizes="32x32" href="{{$SRC_DIR}}/img/favicon/favicon-32x32.png?v=2">
<link rel="icon" type="image/png" sizes="16x16" href="{{$SRC_DIR}}/img/favicon/favicon-16x16.png?v=2">
<link rel="manifest" href="{{$SRC_DIR}}/img/favicon/site.webmanifest?v=2">
<link rel="mask-icon" href="{{$SRC_DIR}}/img/favicon/safari-pinned-tab.svg?v=2" color="#dc272d">
<link rel="shortcut icon" href="{{$SRC_DIR}}/img/favicon/favicon.ico?v=2">
<link rel="apple-touch-icon" sizes="180x180" href="{{$SRC_DIR}}/img/favicon/apple-touch-icon.png?v=2">
<meta name="apple-mobile-web-app-title" content="Molfar Forum">
<meta name="application-name" content="Molfar Forum">
<meta name="msapplication-TileColor" content="#dc272d">
<meta name="msapplication-TileImage" content="{{$SRC_DIR}}/img/favicon/mstile-144x144.png?v=2">
<meta name="msapplication-config" content="{{$SRC_DIR}}/img/favicon/browserconfig.xml?v=2">
<meta name="theme-color" content="#ffffff">
