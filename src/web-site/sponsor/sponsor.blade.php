<li class="sponsors__item-wrapper">
  <a class="sponsors__item loading" href="{{$item->link}}" target="_blank" rel="noopener">{{$item->getTranslatedAttribute('name')}}
    <img class="sponsors__img lazyload" data-src="{{getOptimizedImage( get_file_url($item->image) )}}" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" width="160" alt="{{$item->getTranslatedAttribute('name')}}">
  @if(!empty($item->caption))<p class="sponsors__text">{{$item->getTranslatedAttribute('caption')}}</p>@endif
  </a>
</li>
