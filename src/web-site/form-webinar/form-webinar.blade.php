<form class="form form--webinar" name="webinar" action="{{route('online.checkout', ['slug' => $item->slug])}}" method="POST">
  @csrf
  <input type="hidden" name="webinar_id" value="{{$item->id}}">
  <p class="form__line {{ $errors->has('firstname') ? 'form__line--danger' : '' }}">
    <label class="form__label">Имя
      <input class="form__input" type="text" placeholder="Анастасия" name="firstname" maxlength="255" data-invalid-msg="Введите имя" value="{{ old('firstname') }}" required>
    </label>
    @if($errors->has('firstname'))<span class="form__mistake">{{$errors->first('firstname')}}</span>@endif
  </p>
  <p class="form__line {{ $errors->has('lastname') ? 'form__line--danger' : '' }}">
    <label class="form__label">Фамилия
      <input class="form__input" type="text" placeholder="Ресничкина" data-invalid-msg="Введите фамилию" name="lastname" maxlength="255" value="{{ old('lastname') }}" required>
    </label>
    @if($errors->has('lastname'))<span class="form__mistake">{{$errors->first('lastname')}}</span>@endif
  </p>
  <p class="form__line {{ $errors->has('phone') ? 'form__line--danger' : '' }}">
    <label class="form__label">Телефон
      <input class="form__input" type="tel" placeholder="+380 (00) 000 00 00" name="phone" data-invalid-msg="Введите телефон" value="{{ old('phone') }}" required>
    </label>
    @if($errors->has('phone'))<span class="form__mistake">{{$errors->first('phone')}}</span>@endif
  </p>
  <p class="form__line {{ $errors->has('email') ? 'form__line--danger' : '' }}">
    <label class="form__label">Адрес электронной почты
      <input class="form__input" type="email" placeholder="anastasia@mail.com" name="email" data-invalid-msg="Введите E-mail" value="{{ old('email') }}" maxlength="255" required>
    </label>
    @if($errors->has('email'))<span class="form__mistake">{{$errors->first('email')}}</span>@endif
  </p>
  <p class="form__line">
    <label class="form__label {{ $errors->has('promocode') ? 'form__line--danger' : '' }}">Промокод (если есть)
      <input class="form__input" type="text" placeholder="" name="promocode" value="{{ old('promocode') }}" maxlength="25" data-invalid-msg="Введите промокод">
    </label>
    @if($errors->has('promocode'))<span class="form__mistake">{{$errors->first('promocode')}}</span>@endif
  </p>
  <p class="form__line {{ $errors->has('city') ? 'form__line--danger' : '' }}">
    <label class="form__label">Откуда вы нас смотрите?
      <input class="form__input" type="text" placeholder="Киев" name="city" value="{{ old('city') }}" maxlength="255" data-invalid-msg="Введите город">
    </label>
    @if($errors->has('city'))<span class="form__mistake">{{$errors->first('city')}}</span>@endif
  </p>
  <p class="form__line {{ $errors->has('comment') ? 'form__line--danger' : '' }}">
    <label class="form__label">Комментарий (не обязательно)
      <textarea class="form__textarea" placeholder=":*" rows="5" maxlength="65535" name="comment">{{ old('comment') }}</textarea>
    </label>
    @if($errors->has('comment'))<span class="form__mistake">{{$errors->first('comment')}}</span>@endif
  </p>
  <button class="form__button button button--unfilled"><span>Оплатить</span></button>
</form>
