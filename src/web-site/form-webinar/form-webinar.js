import Validation from '../../common/js/Validation';
import IMask from 'imask';

const rPromocode = /^[a-z0-9\-]$/;

let form = document.forms.webinar;

let mask = new IMask(form.elements.phone, window.phoneMask);

let validation = new Validation(form, {
  classMessage: 'form__mistake',
  classDanger: 'form__line--danger',
  selectorParent: 'p',
  positionMessage: 'beforeend',
});

validation.on('passed', () => {

  var formatedPhone;
  formatedPhone = form.elements.phone.value;
  form.elements.phone.value = mask.unmaskedValue;

  fetch(form.action, {
    method: 'POST',
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    },
    body: new FormData(form)
  })
  .then((response) => response.json())
  .then((response) => {
    form.dispatchEvent(new Event('unlock'));

    if (response.ok) {
      return response;
    } else {
      throw response;
    }
  })
  .then((response) => {
    if(response.paymentData) {
      const wayforpay = new Wayforpay();
      const paymentData = {
        ...response.paymentData,
        straightWidget: true,
        makeRedirect: true,
      };

      wayforpay.run(
        paymentData,
          function (response) {
              // on approved
              alert('Платеж подтвержден');
          },
          function (response) {
              // on declined
              alert('Платеж отменен');
          },
          function (response) {
              // on pending or in processing
              alert('Платеж в обработке');
          }
      );
    } else if(response.redirectUrl) {
      location.href = response.redirectUrl;
    }
  })
  .then((response) => {
    form.reset();
  })
  .catch(function (result) {
    var errors = result.errors;
    console.error(result);
    if (!errors) { return;}
    for (var field in errors) {
      if (errors.hasOwnProperty(field)) {
        validation.flash(field, errors[field].pop());
      }
    }
  });

  form.elements.phone.value = formatedPhone;

});

form.elements.promocode.addEventListener('keypress', (event) => {
  if (!rPromocode.test(
    String.fromCharCode(!event.charCode ? event.which : event.charCode)
  )) {
    event.preventDefault();
    return false;
  }
});
