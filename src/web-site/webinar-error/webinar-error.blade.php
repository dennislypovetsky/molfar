<section class="webinar-error">
  <div class="webinar-error__container container">
    <header class="webinar-error__heading">
      <h1 class="title title--webinar-error webinar-error__title">Не получилось 😢
      </h1>
      @if(isset($exception))
      <p style="display: none">Код ошибки: {{$exception->getCode()}}</p>
      <p style="display: none">{{$exception->getMessage()}}</p>
      @endif
      <p class="webinar-error__text">Платёж был отменен. Попробуйте ещё или свяжитесь с вашим банком.</p>
      <a class="button button--webinar-error" href="{{route('online.webinar',[
        'slug' => $slug
      ])}}"><span>Попробуйте ещё</span>
      </a>
    </header>
  </div>
</section>
