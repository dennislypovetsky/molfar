@extends('web-site.layout.layout')

@section('title', (!empty($item->seo_title) ? $item->getTranslatedAttribute('seo_title') : $item->titleWithPersonName).' — Molfar Online')
@section('pageModifier', 'webinar--buying')
@section('description', $item->autoMetaDescription)

@section('ogImage', !empty($item->og_image) ? url('/storage/'. $item->og_image) : (!empty($item->image) ? url('/storage/'. $item->image) : null))

@section('keywords', $item->getTranslatedAttribute('meta_keywords') )

@section('content')
  @include('web-site.webinar-buying.webinar-buying')
  @include('web-site.form-webinar.form-webinar')
@endsection

@push('scripts')
  <script defer id="widget-wfp-script" language="javascript" type="text/javascript" src="https://secure.wayforpay.com/server/pay-widget.js"></script>
  <script defer src="{{$SRC_DIR}}/js/webinar-buying.min.js?v={{$CACHE_VERSION}}"></script>
@endpush
