<li class="card card--judge judges__card">
  <div class="card__img-wrapper loading"><img class="card__img lazyload" data-srcset="{{getThumbnail($item->person->image,290,290,'fit','top')}} 300w, {{getThumbnail($item->person->image,580,580,'fit','top')}} 600w, {{getThumbnail($item->person->image,870,870,'fit','top')}} 900w" data-src="{{getThumbnail($item->person->image,290,290,'fit','top')}}" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="{{$item->person->getTranslatedAttribute('name')}}"></div>
  <h3 class="card__name">{{$item->person->getTranslatedAttribute('name')}}</h3>
  <p class="card__about">{{$item->getTranslatedAttribute('caption')}}</p>
</li>
