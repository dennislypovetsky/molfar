@extends('web-site.layout.layout')

@section('title', (!empty($item->seo_title) ? $item->getTranslatedAttribute('seo_title') : $item->titleWithPersonName).' — Molfar Online')
@section('pageModifier', 'webinar--after')
@section('description', $item->autoMetaDescription)

@section('ogImage', !empty($item->og_image) ? url('/storage/'. $item->og_image) : (!empty($item->image) ? url('/storage/'. $item->image) : null))

@section('keywords', $item->getTranslatedAttribute('meta_keywords') )

@section('content')
  @include('web-site.webinar-success.webinar-success')
@endsection

@push('scripts')
  {{-- <script defer src="{{$SRC_DIR}}/js/webinar-success.min.js?v={{$CACHE_VERSION}}"></script> --}}
@endpush
