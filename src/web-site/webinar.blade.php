@extends('web-site.layout.layout')

@section('title',  $page->getTranslatedAttribute('seo_title'))
@section('pageModifier', 'page--webinar')
@section('description', $page->getTranslatedAttribute('meta_description'))

@if(!empty($page->image))
  @section('ogImage', url('/storage/'. $page->image) )
@endif
@section('keywords', $page->getTranslatedAttribute('meta_keywords') )

@section('content')
  @include('web-site.webinar.webinar__hero')
  @include('web-site.webinar.webinar__webinars')
@endsection

@push('scripts')
  <script defer src="{{$SRC_DIR}}/js/webinars.min.js?v={{$CACHE_VERSION}}"></script>
@endpush