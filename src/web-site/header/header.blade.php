<?php
$currentRouteName = Route::currentRouteName();
$routeParams = ['year' => $EVENT_YEAR];

$links = [
  ['name' => __('menu.main'), 'slug' => 'main', 'href' => route('main', $routeParams)],
  ['name' => __('menu.schedule'), 'slug' => 'schedule', 'href' => route('schedule', $routeParams)],
  ['name' => __('menu.brow-pro'), 'slug' => 'brow-pro', 'href' => route('brow-pro', $routeParams)],
  ['name' => __('menu.golden-hands'), 'slug' => 'golden-hands', 'href' => route('golden-hands', $routeParams)],
  ['name' => __('menu.foot-profi'), 'slug' => 'foot-profi', 'href' => route('foot-profi', $routeParams)],
  ['name' => __('menu.miss-molfar'), 'slug' => 'miss-molfar', 'href' => route('miss-molfar', $routeParams)],
  ['name' => __('menu.recreation'), 'slug' => 'recreation', 'href' => route('recreation', $routeParams)],
  ['name' => __('menu.dream-team'), 'slug' => 'dream-team', 'href' => route('dream-team', $routeParams)],
  ['name' => __('menu.vote'), 'slug' => 'dream-team.vote', 'href' => route('dream-team.vote', $routeParams)],
  ['name' => __('menu.news'), 'slug' => 'news', 'href' => route('news')],
];

?>
<header class="header">
  <div class="header__container container">
    <nav class="header__nav"><a class="header__link header__link--logo" @if($currentRouteName != 'main')href="{{route('main', $routeParams)}}"@endif>@lang('menu.main')
        <svg class="icon icon-molfar ">
          <use xlink:href="{{$SRC_DIR}}/img/svg/symbol/sprite.svg#molfar"></use>
        </svg></a>
      <ul class="header__list header__list--links">
        @foreach($links as $link)
        @if(!(in_array($link['slug'], $INACTIVE_PAGES) || in_array($EVENT_YEAR.'.'.$link['slug'], $INACTIVE_PAGES)))
        <li class="header__link-wrapper @if($loop->first){{'header__link-wrapper--mobile'}}@endif "> 
          <a class="header__link @if($loop->first){{'header__link--mobile'}}@endif" @if($currentRouteName != $link['slug'])href="{{$link['href']}}"@endif>{{$link['name']}}</a>
        </li>
        @endif
        @endforeach
        <li class="header__link-wrapper">
          <a class="header__link" href="https://online.molfarforum.com">Online</a>
        </li>
      </ul>
    </nav>
  </div>
</header>
