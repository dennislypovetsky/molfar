<section class="webinar-buying">
  <div class="webinar-buying__container container">
    <h1 class="title title--webinar-buying-lector webinar-buying__title-lector">
    @if(isset($item->person_id))Запись на вебинар {{$item->person->genitiveName}}@else{{$item->getTranslatedAttribute('title')}}@endif
    </h1>
    <p class="webinar-buying__text">
    <time class="webinar-buying__date" datetime="{{$item->date}} {{$item->start_at}}">{{date('d.m', strtotime($item->date))}} ({{mb_strtolower(locale_dow(date('w', strtotime($item->date))))}}) в {{date('H:i',strtotime($item->start_at))}}</time>
    </p>
    @if(isset($item->og_image))
    <div class="webinar-buying__img-wrapper loading">
        <img class="webinar-buying__img lazyload"
        data-srcset="{{getThumbnail($item->og_image,360)}} 360w, {{getThumbnail($item->og_image,720)}} 720w, {{getThumbnail($item->og_image,1080)}} 1080w"
        data-src="{{getThumbnail($item->og_image,1080)}}"
        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
        alt="{{$item->title}}"
        role="presentation"/>
    </div>
    @endif
    <h2 class="title title--webinar-buying-topic webinar-buying__topic">{{$item->title}}</h2>
    {!!str_replace('webinar__', 'webinar-buying__', $item->html)!!}
    <p class="webinar-buying__text">Стоимость:<b class="webinar-buying__bold-text"> @if($item->price > 0){{price_uah($item->price)}}@else{{'бесплатно'}}@endif</b>
    </p>
    @if($item->durationInHours > 0)
    <p class="webinar-buying__text">Продолжительность вебинара:<b class="webinar-buying__bold-text"> {{$item->durationInHours}} {{declOfNum($item->durationInHours, ['час', 'часа', 'часов'])}}</b></p>
    @endif
    <div class="webinar-buying__wrapper webinar-buying__wrapper--technical-info"><strong class="webinar-buying__bold-text">Техническая информация</strong>
      <p class="webinar-buying__text">Для просмотра вебинара вам нужен телефон/компьютер/планшет и выход в интернет — хватит даже хорошего 3G/4G, но лучше, конечно, по wi-fi.
      </p>
      <p class="webinar-buying__text">Мы рекомендуем принять участие в прямой трансляции — так вы сможете задавать вопросы лектору. Но если пропустите вебинар, не переживайте — <b class="webinar-buying__bold-text">запись будет доступна ещё 7 дней</b>. Сможете пересмотреть её в удобное для себя время.
      </p>
    </div>
    <div class="webinar-buying__wrapper webinar-buying__wrapper--feedback"><strong class="webinar-buying__bold-text">Обратная связь</strong>
      <p class="webinar-buying__text">Мы очень ценим наших зрителей и всё время стараемся стать лучше. Вы поможете нам в этом, отправив обратную связь на <a class="webinar-buying__link" href="mailto:online@molfarforum.com?subject=Обратная связь" target="_blank" rel="noopener">online@molfarforum.com</a>. Мы будем благодарны любой критике и очень рады добрым словам.
      </p>
      <p class="webinar-buying__text">Вы можете <a class="webinar-buying__link" href="https://www.facebook.com/pg/molfarforum/reviews/" target="_blank" rel="noopener">оставить рекомендацию</a> на нашей Фейсбук-страничке.
      </p>
      <p class="webinar-buying__text">Покажите как вы нас смотрите в своих инста-сторис, тегнув <a class="webinar-buying__link" href="https://www.instagram.com/molfarforum/" target="_blank" rel="noopener">@molfarforum</a> (мы это просто обожаем и лучшие всегда репостим). И подписывайтесь на наш <a class="webinar-buying__link" href="https://www.youtube.com/c/MolfarBeautyForum?sub_confirmation=1" target="_blank" rel="noopener">Ютуб-канал</a>.
      </p>
    </div>
    <div class="webinar-buying__wrapper webinar-buying__wrapper--contact">
      <p class="webinar-buying__text webinar-buying__title">Если возникли вопросы, пишите нам в Мессенджер:
      </p>
      <p class="webinar-buying__text"><a class="webinar-buying__link" href="https://m.me/molfarforum" target="_blank" rel="noopener">m.me/molfarforum</a>
      </p>
      <p class="webinar-buying__text webinar-buying__title">В Директ:
      </p>
      <p class="webinar-buying__text"><a class="webinar-buying__link" href="https://www.instagram.com/molfarforum/" target="_blank" rel="noopener">instagram.com/molfarforum</a>
      </p>
      <p class="webinar-buying__text webinar-buying__title">На почту:
      </p>
      <p class="webinar-buying__text"><a class="webinar-buying__link" href="mailto:online@molfarforum.com" target="_blank" rel="noopener">online@molfarforum.com</a>
      </p>
      <p class="webinar-buying__text webinar-buying__title">Или звоните нашему менеджеру Ирине:
      </p>
      <ul class="webinar-buying__unordered-list webinar-buying__unordered-list--unstyle">
        <li class="webinar-buying__unordered-list-item">
          <p class="webinar-buying__text"><a class="webinar-buying__link" href="tel:+380 (98) 318 71 15" target="_blank" rel="noopener">+380 (98) 318 71 15</a>
          </p>
        </li>
        <li class="webinar-buying__unordered-list-item">
          <p class="webinar-buying__text"><a class="webinar-buying__link" href="tel:+380 (66) 558 31 07" target="_blank" rel="noopener">+380 (66) 558 31 07</a>
          </p>
        </li>
      </ul>
    </div>
  </div>
</section>
