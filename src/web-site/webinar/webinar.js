import Accordion from 'accordion-js';

// var accordion = new Accordion({
//   duration: 600, // animation duration in ms {number}
//   itemNumber: 0, // item number which will be shown {number}
//   aria: true, // add ARIA elements to the HTML structure {boolean}
//   closeOthers: true, // show only one element at the same time {boolean}
//   showItem: false, // always show element that has itemNumber number {boolean}
//   elementClass: 'webinar', // ac
//   questionClass: 'webinar__heading', ac-q
//   answerClass: 'webinar__body', // ac-a
//   targetClass: 'ac-target' // target class {string}
// });

// var accordion = new Accordion({
//   onToggle: function onToggle() { }
// });

new Accordion('.webinars__list', {
  elementClass: 'webinar', // ac
  questionClass: 'webinar__title', // ac-q
  answerClass: 'webinar__body', // ac-a
  duration: 250,
  closeOthers: false
});
