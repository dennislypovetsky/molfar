<section class="webinars">
    <section class="webinars">
    <div class="webinars__container container">
      <h1 class="title title--webinar webinars__title"><span>Предстоящие</span><br><span>вебинары</span>
      </h1>
      <ol class="webinars__list">
        @foreach($items as $item)
          @include('web-site.webinar.webinar__item', ['item'=>$item])
        @endforeach
      </ol>
    </div>
  </section>
