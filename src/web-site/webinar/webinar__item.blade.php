<li class="webinar webinars__item">
  <div class="webinar__header">
    <div class="webinar__heading-wrapper">
      <h3 class="webinar__heading">
      <a class="webinar__title" href="">{{$item->titleWithPersonName}}</a>
      </h3>
    </div>
    <div class="webinar__date-wrapper">
      <time class="webinar__date" datetime="{{$item->date.' '.$item->start_at}}">{{locale_date($item->date, '%d %s')}}</time>
    </div>
    @if(isset($item->cost) && $item->cost > 0)
    <div class="webinar__price-wrapper">
      <p class="webinar__price">{{thin_uah($item->cost)}}</p>
    </div>
    @endif
  </div>
  <div class="webinar__body">
    <div class="webinar__text-wrapper">
      {!!$item->html!!}
    </div>
    <div class="webinar__side-wrapper">
      <div class="webinar__img-wrapper loading">
      <img class="webinar__img lazyload" data-srcset="{{getThumbnail($item->image,900)}} 900w" data-src="{{getThumbnail($item->image,900)}}" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="{{$item->title}}" role="presentation"/>
      </div>
      @if(isset($item->start_at))
      <p class="webinar__text webinar__text--time">
        <time datetime="{{$item->date.' '.$item->start_at}}">{{date('G:i', strtotime($item->start_at))}}@if(isset($item->end_at)) – {{date('G:i', strtotime($item->end_at))}}@endif</time>
      </p>
      @endif
      @if(!empty($item->caption))
      <p class="webinar__text">{{$item->caption}}</p>
      @endif
      <p class="webinar__text webinar__text--featured">7 участников</p>
    </div>
  </div>
  <div class="webinar__button-wrapper">
    <a class="button button--webinar-active webinar__button"><span>Записаться</span></a>
  </div>
</li>
