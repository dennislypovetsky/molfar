<section class="theme">
  <div class="theme__container container">
    <h3 class="theme__title">@lang('dream-team2020-page.theme__title--1') <br>@lang('dream-team2020-page.theme__title--2')</h3>
    <p class="theme__description">@lang('dream-team2020-page.theme__description--1')</p>
    <p class="theme__description">@lang('dream-team2020-page.theme__description--2')</p>
  </div>
</section>
