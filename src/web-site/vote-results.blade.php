@extends('web-site.layout.layout')

@section('title', strtr($page->getTranslatedAttribute('seo_title'), ['{$teamName}'=>$team->name]) )

@section('description',  strtr($page->getTranslatedAttribute('meta_description'), ['{$teamName}'=>$team->name]))

@if(!empty($page->image))

  @section('ogImage', url('/storage/'. $page->image) )

@endif

@section('pageModifier', 'page--vote-results')

@section('keywords', $page->getTranslatedAttribute('meta_keywords'))

@section('content')
  @include('web-site.vote-results.vote-results')
@endsection

@push('scripts')
   {{-- <script defer src="{{$SRC_DIR}}/js/vote-results.min.js?v={{$CACHE_VERSION}}"></script> --}}
@endpush
