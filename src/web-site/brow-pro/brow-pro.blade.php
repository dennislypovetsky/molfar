
        <section class="brow-pro-hero">
          <div class="brow-pro-hero__wrapper brow-pro-hero__wrapper--img">
          </div>
          <div class="brow-pro-hero__wrapper brow-pro-hero__wrapper--heading container">
            <header class="heading brow-pro-hero__heading">
              <h1 class="heading__title"><span class="heading__text heading__text--title">Brow Pro</span><br/><span class="heading__text">Molfar ‘22</span>
              </h1>
              <p class="heading__text">чемпионат по моделированию и окрашиванию бровей в рамках международного форума Molfar
              </p>
              <div class="heading__wrapper heading__wrapper--icon">
                <svg class="icon icon--brow-pro" width="98" height="40">
                  <use xlink:href="{{$SRC_DIR}}/img/svg/symbol/sprite__bro-pro.svg#brow-pro"></use>
                </svg>
              </div>
            </header>
          </div>
          <div class="brow-pro-hero__wrapper brow-pro-hero__wrapper--icon">
            <svg class="icon icon--brow-pro" width="196" height="80">
              <use xlink:href="{{$SRC_DIR}}/img/svg/symbol/sprite__bro-pro.svg#brow-pro"></use>
            </svg>
          </div>
        </section>
        <section class="motivation">
          <div class="motivation__container container">
            <header class="heading motivation__heading">
              <h2 class="heading__title">Главный чемпионат среди бровистов
              </h2>
            </header>
            <div class="motivation__wrapper motivation__wrapper--img">
              <div class="motivation__img-wrapper lazyload">
              </div>
            </div>
            <div class="motivation__wrapper motivation__wrapper--text">
              <h3 class="motivation__title">Заявите о себе
              </h3>
              <p class="motivation__text">Brow Pro Molfar даёт возможность не только получить хороший денежный приз, а также проявить себя как специалиста и познакомиться с коллегами
              </p>
              <p class="motivation__text">Чемпиону Brow Pro Molfar будут оплачены <b class="motivation__feat feat">участие в Bro Pro 2021</b> c дорогой и проживанием
              </p><a class="motivation__link motivation__link--icon" href="https://youtu.be/MGcJ378Mb1g" target="_blank" rel="noopener">
              <svg class="icon icon--youtube" width="28" height="20">
                <use xlink:href="{{$SRC_DIR}}/img/svg/symbol/sprite__bro-pro.svg#youtube"></use>
              </svg>Bro Pro 2018</a>
            </div>
            <div class="motivation__wrapper motivation__wrapper--list">
              <h3 class="motivation__title">Что необходимо для участия
              </h3>
              <ul class="motivation__list"><li>
                <p class="motivation__text">Зарегистрироваться по номеру телефона
                </p></li><li>
                <p class="motivation__text">Оплатить взнос
                </p></li><li><a class="motivation__link" href="https://drive.google.com/file/d/1Mf_c-xnMBx6E6iGFAe4o3egnya9J2cOc/view?usp=sharing" target="_blank" rel="noopener">Изучить правила</a></li>
              </ul>
            </div>
          </div>
        </section>
        <section class="rules">
          <div class="rules__container container">
            <header class="heading rules__heading">
              <h2 class="heading__title">Правила участия
              </h2>
            </header>
            <div class="rules__wrapper">
              <div class="rules__inner">
                <h4 class="rules__title">Общие правила
                </h4>
              </div>
              <ul class="rules__list"><li>
                <p class="rules__text">Вам нужно иметь с собой всё необходимое для выполнения конкурсной работы
                </p></li><li>
                <p class="rules__text">Допускается только классический метод моделирования формы бровей с использованием пинцета для бровей
                </p></li><li>
                <p class="rules__text">Окрашивание выполняется только краской Elan professional line
                </p></li><li>
                <p class="rules__text">Модель вам предоставит организатор
                </p></li>
              </ul>
            </div>
            <div class="rules__wrapper">
              <div class="rules__inner">
                <h4 class="rules__title">Требования к внешнему виду специалиста
                </h4>
              </div>
              <ul class="rules__list"><li>
                <p class="rules__text">Собранные волосы
                </p></li><li>
                <p class="rules__text">Чёрная футболка (предоставляет организатор)
                </p></li><li>
                <p class="rules__text">Чёрные брюки или юбка ниже колен офисного стиля
                </p></li><li>
                <p class="rules__text">Сменная обувь с закрытым носком чёрного цвета
                </p></li><li>
                <p class="rules__text">Отсутствие ярких аксессуаров
                </p></li><li>
                <p class="rules__text">Ознакомьтесь с <a class="rules__link" href="https://drive.google.com/file/d/1Mf_c-xnMBx6E6iGFAe4o3egnya9J2cOc/view?usp=sharing" target="_blank" rel="noopener">полными правила чемпионата</a>
                </p></li>
              </ul>
            </div>
          </div>
        </section>
        <section class="assessment">
          <div class="assessment__container container">
            <header class="heading assessment__heading">
              <h2 class="heading__title">Оценивание
              </h2>
            </header>
            <div class="assessment__wrapper assessment__wrapper--callout">
              <p class="assessment__text"><i>В оценке чемпионата есть три критерия. Каждый критерий оценивается по 10-балльной системе</i>
              </p>
            </div>
            <div class="assessment__wrapper">
              <h4 class="assessment__title">Общее впечатление
              </h4>
              <p class="assessment__text">ДО и ПОСЛЕ моделирования и окрашивания бровей, эстетический вид модели
              </p>
            </div>
            <div class="assessment__wrapper">
              <h4 class="assessment__title">Форма
              </h4>
              <p class="assessment__text">симметричность и соответствие формы бровей лицу
              </p>
            </div>
            <div class="assessment__wrapper">
              <h4 class="assessment__title">Цвет
              </h4>
              <p class="assessment__text">выбор цвета и гармоничный цвет бровей
              </p>
            </div>
          </div>
        </section>
        <section class="prize">
          <div class="prize__wrapper prize__wrapper--img">
            <div class="prize__img-wrapper lazyload">
            </div>
          </div>
          <div class="prize__container container">
            <div class="prize__inner">
              <header class="heading prize__heading">
                <h2 class="heading__title">Призовой фонд
                </h2>
              </header>
              <div class="prize__wrapper">
                <p class="prize__title"><b>I место</b>
                </p>
                <p class="prize__text"><b class="prize__feat feat">$1000</b> и подарки от спонсоров а также оплаченное участие <b class="prize__feat feat">в Bro Pro 2021</b>
                </p>
              </div>
              <div class="prize__wrapper">
                <p class="prize__title"><b>II место</b>
                </p>
                <p class="prize__text"><b class="prize__feat feat">$500</b> и подарки от спонсоров
                </p>
              </div>
              <div class="prize__wrapper">
                <p class="prize__title"><b>III место</b>
                </p>
                <p class="prize__text"><b class="prize__feat feat">$300</b> и подарки от спонсоров
                </p>
              </div>
            </div>
          </div>
        </section>
        <section class="price">
          <div class="price__container container">
            <header class="heading price__heading">
              <h2 class="heading__title">Стоимость
              </h2>
            </header>
            <div class="price__wrapper">
              <p class="price__title"><b class="price__feat feat">8&#8201;900 ₴</b>
              </p>
              <p class="price__text price__text--description">участие<span>билет на Molfar Forum</span>
              </p>
              <p class="price__text">включает жильё и питание
              </p>
            </div>
            <div class="price__wrapper">
              <p class="price__title"><b class="price__feat feat">1&#8201;400 ₴</b>
              </p>
              <p class="price__text price__text--description">участие
              </p>
              <p class="price__text">о жилье и питании придётся заботиться самостоятельно
              </p>
            </div>
          </div>
        </section>
        <section class="registration">
          <div class="registration__container container">
            <div class="registration__wrapper registration__wrapper--img">
              <div class="registration__img-wrapper lazyload">
              </div>
            </div>
            <header class="heading registration__heading">
              <div class="heading__wrapper">
                <h2 class="heading__title">Регистрация
                </h2>
                <p class="heading__text">регистрация открыта до 1 сентября
                </p>
              </div>
              <div class="heading__wrapper">
                <p class="heading__text"><a class="heading__link" href="tel:+380504207887">+380-50-420-78-87</a>
                </p>
                <p class="heading__text"><i>Академия Карьеры Салонного Бизнеса</i><br/><i>«Руки-Ножницы»</i>
                </p>
              </div>
            </header>
          </div>
        </section>
