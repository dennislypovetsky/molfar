@extends('web-site.layout-miss.layout')

@section('title', $page->getTranslatedAttribute('seo_title'))

@section('pageModifier', 'page--index')

@if(!empty($page->image))

  @section('ogImage', url('/storage/'. $page->image) )

@endif

@section('description', $page->getTranslatedAttribute('meta_description'))

@section('keywords', $page->getTranslatedAttribute('meta_keywords'))

@section('content')
    @include('web-site.miss-molfar.miss-molfar')

@endsection
