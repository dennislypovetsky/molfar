
        <section class="hero hero--index">
          <div class="hero__container container">
            <header class="heading hero__heading">
              <h1 class="heading__title">Miss Molfar
              </h1>
              <p class="heading__text">Конкурс красоты<br/>для участниц beauty-форума Molfar
              </p><a class="button button--white" href="#registration">Принять участие</a>
            </header>
          </div>
        </section>
        <section class="org">
          <div class="org__container container">
            <svg class="icon icon--sff_logo" width="340" height="70">
              <use xlink:href="{{$SRC_DIR}}/img/svg/symbol/sprite__miss-molfar.svg#sff_logo"></use>
            </svg>
            <header class="heading org__heading">
              <p class="heading__text"><b>Организатор Miss Molfar</b> — модельное агентство Star Face Family.
              </p>
              <p class="heading__text">Один из основных проектов агентства — имиджевый конкурс красоты и моды <b>Star Face of the Season</b>
              </p>
            </header>
            <div class="org__wrapper">
              <div class="org__img-wrapper lazyload">
              </div>
            </div>
          </div>
          <svg class="moving-text moving-text--org" id="text-container" viewBox="0 0 500 96" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path id="text-curve" d="M0 79.5h1203"></path>
            <text y="0" font-size="96">
              <textpath class="moving-text__textpath moving-text__textpath--org" id="text-path" href="#text-curve" startOffset="200">Star Face Family
              </textpath>
            </text>
          </svg>
        </section>
        <section class="for-you">
          <div class="for-you__container container">
            <header class="heading for-you__heading">
              <h2 class="heading__title">Чего ждать от конкурса
              </h2>
            </header>
            <ul class="for-you__list">
              <li class="for-you__wrapper for-you__wrapper--desktop loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (1)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (1)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (1)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (1)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--cover loading">
                <p class="for-you__text">Работа с профессиональными стилистами, парикмахерами и визажистами
                </p><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (2)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (2)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (2)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (2)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Работа с профессиональными стилистами, парикмахерами и визажистами" title=""/>
              </li>
              <li class="for-you__wrapper loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (3)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (3)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (3)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (3)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--cover loading">
                <p class="for-you__text">Профессиональные фотосессии
                </p><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (4)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (4)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (4)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (4)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Профессиональные фотосессии" title=""/>
              </li>
              <li class="for-you__wrapper loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (6)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (6)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (6)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (6)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--cover loading">
                <p class="for-you__text">Тысячи лайков в Instagram
                </p><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (5)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (5)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (5)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (5)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Тысячи лайков в Instagram" title=""/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--desktop loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (7)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (7)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (7)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (7)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--desktop loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (8)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (8)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (8)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (8)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (9)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (9)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (9)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (9)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--desktop loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (10)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (10)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (10)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (10)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--cover loading">
                <p class="for-you__text">Признание от комьюнити Beauty-форума Molfar
                </p><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (11)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (11)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (11)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (11)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Признание от комьюнити Beauty-форума Molfar" title=""/>
              </li>
              <li class="for-you__wrapper loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (12)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (12)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (12)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (12)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--cover loading">
                <p class="for-you__text">Призы и подарки для каждой участницы
                </p><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (13)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (13)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (13)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (13)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Призы и подарки для каждой участницы" title=""/>
              </li>
              <li class="for-you__wrapper loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (14)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (14)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (14)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (14)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--desktop loading"><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (15)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (15)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (15)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (15)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="" role="presentation"/>
              </li>
              <li class="for-you__wrapper for-you__wrapper--cover loading">
                <p class="for-you__text">Вечеринка-награждение
                </p><img class="for-you__img lazyload" data-srcset="{{$SRC_DIR}}/img/for-you/img/for-you (16)_720.jpg 360w, {{$SRC_DIR}}/img/for-you/img/for-you (16)_1080.jpg 720w, {{$SRC_DIR}}/img/for-you/img/for-you (16)_1280.jpg 1080w" data-src="{{$SRC_DIR}}/img/for-you/img/for-you (16)_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Вечеринка-награждение" title=""/>
              </li>
            </ul>
          </div>
        </section>
        <section class="procedure">
          <div class="procedure__container container">
            <header class="heading procedure__heading">
              <h2 class="heading__title">Конкурс проходит в три этапа
              </h2>
            </header>
            <ol class="procedure__list">
              <li class="procedure__wrapper">
                <p class="procedure__text procedure__text--stage">Первый этап
                </p>
                <p class="procedure__text procedure__text--title">Отборочный
                </p>
                <p class="procedure__text">Онлайн-отбор по заполеным анкетам на сайте. Количество допущеных участниц определяет жюри
                </p>
              </li>
              <li class="procedure__wrapper">
                <p class="procedure__text procedure__text--stage">Второй этап
                </p>
                <p class="procedure__text procedure__text--title">Подготовительный
                </p>
                <p class="procedure__text">Проработка образов для участниц стилистами конкурса. Постановка дефиле и сценических выходов
                </p>
              </li>
              <li class="procedure__wrapper">
                <p class="procedure__text procedure__text--stage">Третий этап
                </p>
                <p class="procedure__text procedure__text--title">Финальный
                </p>
                <p class="procedure__text">Проводится конкурсные дефиле, оценка независимым жюри и вечеринка-награждение
                </p>
              </li>
            </ol>
          </div>
        </section>
        <section class="for-whom">
          <div class="for-whom__container container">
            <header class="heading for-whom__heading">
              <h2 class="heading__title">От вас нужно только участие
              </h2>
            </header>
            <div class="for-whom__wrapper">
              <p class="for-whom__text">Мы предоставим вам визажистов, парикмахеров и стилистов конкурса для проработки образов, а также одежду. К участницам есть только два требования: возраст 18-40 лет и рост от 165 сантиметров
              </p><a class="for-whom__link" href="#registration">Заполняйте анкету</a>
            </div>
            <div class="for-whom__wrapper for-whom__wrapper--img">
              <div class="for-whom__img-wrapper lazyload">
              </div>
            </div>
          </div>
          <svg class="moving-text moving-text--for-whom" id="text-container" viewBox="0 0 500 96" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path id="text-curve" d="M0 79.5h1203"></path>
            <text y="0" font-size="96">
              <textpath class="moving-text__textpath moving-text__textpath--for-whom" id="text-path" href="#text-curve" startOffset="200">Miss Molfar
              </textpath>
            </text>
          </svg>
        </section>
        <section class="how-it-look">
          <div class="how-it-look__container container">
            <header class="heading how-it-look__heading">
              <h2 class="heading__title">Как это выглядит
              </h2>
            </header>
            <ol class="how-it-look__list">
              <li class="how-it-look__wrapper">
                <div class="how-it-look__inner">
                  <p class="how-it-look__text how-it-look__text--stage">Первое дефиле
                  </p>
                  <p class="how-it-look__text how-it-look__text--title">Национальная одежда
                  </p>
                </div>
                <div class="how-it-look__img-wrapper loading"><img class="how-it-look__img lazyload" data-srcset="{{$SRC_DIR}}/img/how-it-look/img/national-dress_720.jpg 360w, {{$SRC_DIR}}/img/how-it-look/img/national-dress_1080.jpg 720w, {{$SRC_DIR}}/img/how-it-look/img/national-dress_1440.jpg 1080w" data-src="{{$SRC_DIR}}/img/how-it-look/img/national-dress_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Первое дефиле" title=""/>
                </div>
              </li>
              <li class="how-it-look__wrapper">
                <div class="how-it-look__inner">
                  <p class="how-it-look__text how-it-look__text--stage">Второе дефиле
                  </p>
                  <p class="how-it-look__text how-it-look__text--title">Профессиональный лук
                  </p>
                </div>
                <div class="how-it-look__img-wrapper loading"><img class="how-it-look__img lazyload" data-srcset="{{$SRC_DIR}}/img/how-it-look/img/professional-look_720.jpg 360w, {{$SRC_DIR}}/img/how-it-look/img/professional-look_1080.jpg 720w, {{$SRC_DIR}}/img/how-it-look/img/professional-look_1440.jpg 1080w" data-src="{{$SRC_DIR}}/img/how-it-look/img/professional-look_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Второе дефиле" title=""/>
                </div>
              </li>
              <li class="how-it-look__wrapper">
                <div class="how-it-look__inner">
                  <p class="how-it-look__text how-it-look__text--stage">Финальное дефиле
                  </p>
                  <p class="how-it-look__text how-it-look__text--title">Вечерний выход
                  </p>
                </div>
                <div class="how-it-look__img-wrapper loading"><img class="how-it-look__img lazyload how-it-look__img how-it-look__img--evening-dress" data-srcset="{{$SRC_DIR}}/img/how-it-look/img/evening-dress_720.jpg 360w, {{$SRC_DIR}}/img/how-it-look/img/evening-dress_1080.jpg 720w, {{$SRC_DIR}}/img/how-it-look/img/evening-dress_1440.jpg 1080w" data-src="{{$SRC_DIR}}/img/how-it-look/img/evening-dress_720.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Финальное дефиле" title=""/>
                </div>
              </li>
            </ol>
          </div>
        </section>
        <section class="nomination">
          <div class="nomination__container container">
            <div class="nomination__img-wrapper"><img class="nomination__img" src="{{$SRC_DIR}}/img/nomination/img/tiara-diadem_fixed.png" alt="Тиара" title=""/>
            </div>
            <ol class="nomination__list">
              <li class="nomination__wrapper nomination__wrapper--main-title">
                <p class="nomination__text nomination__text--nomination"><b>Главная номинация</b>
                </p>
                <p class="nomination__text nomination__text--title">Miss Molfar
                </p>
              </li>
              <li class="nomination__wrapper">
                <p class="nomination__text nomination__text--nomination"><b>Вторая номинация</b>
                </p>
                <p class="nomination__text nomination__text--title">Miss Molfar 1st runner up
                </p>
              </li>
              <li class="nomination__wrapper">
                <p class="nomination__text nomination__text--nomination"><b>Третья номинация</b>
                </p>
                <p class="nomination__text nomination__text--title">Miss Molfar 2nd runner up
                </p>
              </li>
              <li class="nomination__wrapper">
                <p class="nomination__text nomination__text--nomination"><b>Четвёртая номинация</b>
                </p>
                <p class="nomination__text nomination__text--title">Miss Molfar Audience Choice Award
                </p>
              </li>
              <li class="nomination__wrapper">
                <p class="nomination__text nomination__text--nomination"><b>Пятая номинация</b>
                </p>
                <p class="nomination__text nomination__text--title">Miss Molfar Internet Audience Choice Award
                </p>
              </li>
            </ol>
            <div class="nomination__wrapper nomination__wrapper--special">
              <p class="nomination__text nomination__text--special"><b>Подарки для каждой участницы</b>
              </p>
              <p class="nomination__text">у наших спонсоров есть много подарков для вас, так что ни одна учасница не останется без вознаграждения
              </p>
            </div>
          </div>
        </section>
        <section class="registration" id="registration">
          <div class="registration__container container">
            <header class="heading registration__heading">
              <h2 class="heading__title">Регистрируйтесь
              </h2>
            </header>
            <form class="form registration__form" id="regForm">
              <p class="form__success">Отправлено!
              </p>
              <p class="form__line"><input class="form__input" type="text" placeholder="Имя" name="firstname" id="firstname" required="required"/>
                <label class="form__label" for="firstname">Имя
                </label>
              </p>
              <p class="form__line"><input class="form__input" type="text" placeholder="Фамилия" name="lastname" id="lastname" required="required"/>
                <label class="form__label" for="lastname">Фамилия
                </label>
              </p>
              <p class="form__line"><input class="form__input" type="date" placeholder="Дата рождения" name="birth-date" id="birth-date" required="required"/>
                <label class="form__label" for="birth-date">Дата рождения
                </label>
              </p>
              <p class="form__line"><input class="form__input" type="text" placeholder="Город" name="city" id="city" required="required"/>
                <label class="form__label" for="city">Город
                </label>
              </p>
              <p class="form__line"><input class="form__input" type="text" placeholder="Ссылка на Instagram" name="ig-link" id="ig-link" required="required"/>
                <label class="form__label" for="ig-link">Ссылка на Instagram
                </label>
              </p>
              <p class="form__line"><input class="form__input" type="tel" placeholder="Телефон" name="tel" id="tel" required="required"/>
                <label class="form__label" for="tel">Телефон
                </label>
              </p>
              <p class="form__line"><input class="form__input" type="email" placeholder="Э-почта" name="email" id="email" required="required"/>
                <label class="form__label" for="email">Э-почта
                </label>
              </p>
              <p class="form__line"><input class="form__input" type="text" placeholder="Принимали участие в конкурсах красоты?" name="expirience" id="expirience" required="required"/>
                <label class="form__label" for="expirience">Опыт в конкурсах красоты?
                </label>
              </p>
              <p class="form__line"><input class="form__file" type="file" id="file" accept="image/*" multiple="multiple" required="required" name="images[]"/>
                <label class="form__label form__label--file" for="file">Добавьте 2 фото — в полный рост и портрет
                </label>
              </p>
              <p class="form__line form__line--checkbox"><input class="form__checkbox" type="checkbox" id="checkbox" required="required" checked="checked"/>
                <label class="form__label form__label--checkbox" for="checkbox">Я ознакомлена с <a class="form__link" href="https://drive.google.com/file/d/10BkyfSRmUgiWYQrqhe6qRN91Q6L8QzFC/view?usp=sharing" target="_blank" rel="noopener">правилами конкурса</a> и даю разрешение на обработку моих персональных данных
                </label>
              </p>
              <button class="button button--red form__button"><span>Подать заявку</span>
              </button>
            </form>
            <div class="registration__wrapper registration__wrapper--img lazyload">
            </div>
          </div>
        </section>
