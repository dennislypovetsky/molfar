@component('client-email.index.index',['footer_caption'=>'Online Molfarforum', 'footer_link'=>'https://online.molfarforum.com/','footer_email'=>'online@molfarforum.com'])
@slot('header_image')
<p style="Margin:0;Margin-bottom:35px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.125;margin:0;margin-bottom:35px;padding:0;text-align:left"></p>
@endslot
{!!$body!!}
<p style="Margin:0;Margin-bottom:35px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.125;margin:0;margin-bottom:35px;padding:0;text-align:left">
  @lang('emails.questions')</p>
<p class="p-last-child" style="Margin:0;Margin-bottom:10px;color:#333;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.125;margin:0;margin-bottom:10px;padding:0;text-align:left">
  @lang('emails.team-molfar')</p>

@endcomponent
