@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(isset($dataTypeContent->id)){{ route('voyager.webinars.update', $dataTypeContent->id) }}@else{{ route('voyager.webinars.store') }}@endif" method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <!-- ### TITLE ### -->
                    <div class="panel">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-heading">
                            <h3 class="panel-title">Вебинар</h3>
                        </div>
                        <div class="panel-body">
                          <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="title">Заголовок</label>
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'title',
                                '_field_trans' => get_field_translations($dataTypeContent, 'title')
                            ])
                            <input type="text" class="form-control" id="title" name="title" placeholder="{{old('title', $dataTypeContent->title ?? '')}}" value="{{old('title', $dataTypeContent->title ?? '')}}">
                          </div>
                          <div class="form-group">
                              <label for="person_id">Лектор</label>
                              <select class="form-control select2" name="person_id">
                                @php
                                  $model = app('App\Person');
                                  $query = $model::select('id','name')->get();
                                @endphp
                                <option value="" @if(!isset($dataTypeContent->person_id)){{ 'selected="selected"' }}@endif>Не задано</option>
                                @foreach($query as $relationshipData)
                                  <option value="{{ $relationshipData->id }}" @if($dataTypeContent->person_id == $relationshipData->id){{ 'selected="selected"' }}@endif>{{ $relationshipData->name }}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                    </div>

                    <!-- ### CONTENT ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Анонс</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>

                        <div class="panel-body">

                            @include('voyager::multilingual.input-hidden', [
                              '_field_name'  => 'md',
                              '_field_trans' => get_field_translations($dataTypeContent, 'md')
                            ])
                            <textarea class="form-control" name="md" id="markdownbody">@if(isset($dataTypeContent->md)){{ old('md', $dataTypeContent->md) }}@else{{ old('md') }}@endif</textarea>

                        </div>
                    </div><!-- .panel -->

                </div>
                <div class="col-md-4">
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-dark">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> Свойства</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-up" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                                <label for="date">Дата вебинара</label>
                                <div class='input-group date'>
                                  <input type="date" data-datepicker='{"format": "YYYY-MM-DD"}' class="form-control" name="date"
                                  value="@if(isset($dataTypeContent->date)){{ \Carbon\Carbon::parse(old('date', $dataTypeContent->date))->format('Y-m-d') }}@else{{old('date')}}@endif">
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('start_at') ? 'has-error' : '' }}">
                                <label for="start_at">Время начала</label>
                                <div class='input-group date'>
                                  <input type="time" data-name="Время начала" class="form-control" name="start_at" data-datepicker='{"format": "HH:mm:ss"}' value="{{ old('start_at', $dataTypeContent->start_at ?? '') }}">
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                  </span>
                                </div>
                              </div>
                            <div class="form-group {{ $errors->has('end_at') ? 'has-error' : '' }}">
                                <label for="end_at">Время окончания</label>
                                <div class='input-group date'>
                                  <input type="time" data-name="Время окончания" class="form-control" name="end_at" value="{{ old('end_at', $dataTypeContent->end_at ?? '') }}" data-datepicker='{"format": "HH:mm:ss"}'>
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-time"></span>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('cost') ? 'has-error' : '' }}">
                                <label for="cost">Стоимость</label>
                                <div class="input-group">
                                  <input type="number" step="0.01" min="0" class="form-control" name="cost" value="{{ old('cost', $dataTypeContent->cost ?? '') }}">
                                  <span class="input-group-addon">₴</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status">Статус</label>
                                <select class="form-control" name="status">
                                    <option value="PUBLISHED"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PUBLISHED') selected="selected"@endif>{{ __('voyager::post.status_published') }}</option>
                                    <option value="DRAFT"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'DRAFT') selected="selected"@endif>{{ __('voyager::post.status_draft') }}</option>
                                    <option value="PENDING"@if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PENDING') selected="selected"@endif>{{ __('voyager::post.status_pending') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="panel-footer clearfix">
                            <button type="submit" class="btn btn-primary pull-right">
                                @if(isset($dataTypeContent->id))Обновить@else <i class="icon wb-plus-circle"></i> Сохранить @endif
                            </button>
                        </div>
                    </div>

                    <!-- ### Caption ### -->
                    <?php
                    $isEmptyCaption = empty($dataTypeContent->caption);
                    ?>
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> Примечание</h3>
                            <div class="panel-actions">
                            <a class="panel-action @if($isEmptyCaption){{'panel-collapsed voyager-angle-down'}}@else{{'voyager-angle-up'}}@endif" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body" @if($isEmptyCaption)style="display:none"@endif>
                            <div class="form-group {{ $errors->has('caption') ? 'has-error' : '' }}">
                                <textarea class="form-control" name="caption">@if(!$isEmptyCaption){{ $dataTypeContent->caption }}@endif</textarea>
                            </div>
                        </div>
                    </div>

                    <!-- ### IMAGE ### -->
                    <div class="panel panel-bordered">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> Изображение</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-up" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if(isset($dataTypeContent->image))
                                <img src="{{ filter_var($dataTypeContent->image, FILTER_VALIDATE_URL) ? $dataTypeContent->image : Voyager::image( $dataTypeContent->image ) }}" style="width:100%" />
                            @endif
                            <input type="file" name="image">
                        </div>
                    </div>

                    <!-- ### SEO CONTENT ### -->
                    <div class="panel panel-bordered panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> SEO</h3>
                            <div class="panel-actions">
                                <a class="panel-action panel-collapsed voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body" style="display:none">
                            <div class="form-group {{ $errors->has('og_image') ? 'has-error' : '' }}">
                                <label for="og_image">OG-image</label>
                                @if(isset($dataTypeContent->og_image))
                                <img src="{{ filter_var($dataTypeContent->og_image, FILTER_VALIDATE_URL) ? $dataTypeContent->og_image : Voyager::image( $dataTypeContent->og_image ) }}" style="width:100%" />
                                @endif
                                <input type="file" name="og_image">
                            </div>
                            <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                <label for="slug">Ссылка</label>
                                <input type="text" class="form-control" id="slug" name="slug"
                                    placeholder="{{old('slug', $dataTypeContent->slug ?? '')}}"
                                    {{!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}}
                                    value="@if(isset($dataTypeContent->slug)){{ $dataTypeContent->slug }}@endif">
                            </div>
                            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                                <label for="meta_description">{{ __('voyager::post.meta_description') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_description',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_description')
                                ])
                                <textarea class="form-control" name="meta_description">@if(isset($dataTypeContent->meta_description)){{ $dataTypeContent->meta_description }}@endif</textarea>
                            </div>
                            <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                                <label for="meta_keywords">{{ __('voyager::post.meta_keywords') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_keywords',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords')
                                ])
                                <textarea class="form-control" name="meta_keywords">@if(isset($dataTypeContent->meta_keywords)){{ $dataTypeContent->meta_keywords }}@endif</textarea>
                            </div>
                            <div class="form-group {{ $errors->has('seo_title') ? 'has-error' : '' }}">
                                <label for="seo_title">{{ __('voyager::post.seo_title') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'seo_title',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'seo_title')
                                ])
                                <input type="text" class="form-control" name="seo_title" value="@if(isset($dataTypeContent->seo_title)){{ $dataTypeContent->seo_title }}@endif">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')
<script>
  var rYouTubeId = /^[a-zA-Z0-9_\-]{11}$/;

  function _replaceSelection(cm, active, startEnd, url) {
    if(/editor-preview-active/.test(cm.getWrapperElement().lastChild.className))
      return;

    var text;
    var start = startEnd[0];
    var end = startEnd[1];
    var startPoint = cm.getCursor("start");
    var endPoint = cm.getCursor("end");
    if(url) {
      end = end.replace("#url#", url);
    }
    if(active) {
      text = cm.getLine(startPoint.line);
      start = text.slice(0, startPoint.ch);
      end = text.slice(startPoint.ch);
      cm.replaceRange(start + end, {
        line: startPoint.line,
        ch: 0
      });
    } else {
      text = cm.getSelection();
      cm.replaceSelection(start + text + end);

      startPoint.ch += start.length;
      if(startPoint !== endPoint) {
        endPoint.ch += start.length;
      }
    }
    cm.setSelection(startPoint, endPoint);
    cm.focus();
  }

  function YouTubeGetID(url){
    var ID = '';
    url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    if(url[2] !== undefined) {
      ID = url[2].split(/[^0-9a-z_\-]/i);
      ID = ID[0];
    }
    else {
      ID = url;
    }
      return ID;
  }


$('document').ready(function () {
  $('#slug').slugify();

  var markdownbody = document.getElementById("markdownbody");

  var simplemde = new SimpleMDE({
    element: markdownbody,
    forceSync: true,
    insertTexts: {
      horizontalRule: ["", "\n\n-----\n\n"],
      image: ["![Вставьте подпись к картинке сюда](Загрузите картинку в Галерею, скопируйте («Click Here» в свойствах файла) и вставьте ссылку сюда, вместо этого текста", ")"],
      link: ["[", "](http://)"],
      table: ["", "\n\n| Column 1 | Column 2 | Column 3 |\n| -------- | -------- | -------- |\n| Text     | Text      | Text     |\n\n"],
    },
    toolbar: [

      {
        name: "bold",
        action: SimpleMDE.toggleBold,
        className: "fa fa-bold",
        title: "Жирный",
        default: true
      },
      {
        name: "italic",
        action: SimpleMDE.toggleItalic,
        className: "fa fa-italic",
        title: "Курсив",
        default: true
      },
      {
        name: "strikethrough",
        action: SimpleMDE.toggleStrikethrough,
        className: "fa fa-strikethrough",
        title: "Перечеркнутый"
      },
      {
        name: "heading-2",
        action: SimpleMDE.toggleHeading2,
        className: "fa fa-header fa-header-x fa-header-2",
        title: "Подзаголовок"
      },
      "|",
      {
        name: "quote",
        action: SimpleMDE.toggleBlockquote,
        className: "fa fa-quote-left",
        title: "Цитата",
        default: true
      },
      {
        name: "unordered-list",
        action: SimpleMDE.toggleUnorderedList,
        className: "fa fa-list-ul",
        title: "Ненумерованный список",
        default: true
      },
      {
        name: "ordered-list",
        action: SimpleMDE.toggleOrderedList,
        className: "fa fa-list-ol",
        title: "Нумерованный список",
        default: true
      },
      // {
      //   name: "clean-block",
      //   action: SimpleMDE.cleanBlock,
      //   className: "fa fa-eraser fa-clean-block",
      //   title: "Clean block"
      // },
      "|",
      {
        name: "link",
        action: SimpleMDE.drawLink,
        className: "fa fa-link",
        title: "Ссылка",
        default: true
      },
      {
        name: "image",
        action: SimpleMDE.drawImage,
        className: "fa fa-picture-o",
        title: "Загрузите картинку в Галерею, скопируйте («Click Here» в свойствах файла) и вставьте ссылку",
        default: true
      },
      {
        name: "youtube",
        action: function(editor){


          var cm = editor.codemirror;
          var stat = editor.getState(cm);
          var options = editor.options;
          var url = "http://", input;

          input = prompt('Ссылка на YouTube-видео https://www.youtube.com/watch?v=[ID видео]', 'https://');

          var videoId =  YouTubeGetID(input);

          if(!rYouTubeId.test(videoId)) return alert('Неверная ссылка');

          url = 'https://www.youtube.com/watch?v='+videoId;

          _replaceSelection(cm, stat.image, ["![](", "#url#)"], url);

        },
        className: "fa fa-youtube-play",
        title: "Вставить видео Youtube",
        default: true
      },
      // {
      //   name: "table",
      //   action: SimpleMDE.drawTable,
      //   className: "fa fa-table",
      //   title: "Insert Table"
      // },
      // {
      //   name: "horizontal-rule",
      //   action: SimpleMDE.drawHorizontalRule,
      //   className: "fa fa-minus",
      //   title: "Insert Horizontal Line"
      // },
      "|",
      {
        name: "preview",
        action: SimpleMDE.togglePreview,
        className: "fa fa-eye no-disable",
        title: "Toggle Preview",
        default: true
      },
      {
        name: "side-by-side",
        action: SimpleMDE.toggleSideBySide,
        className: "fa fa-columns no-disable no-mobile",
        title: "Toggle Side by Side",
        default: true
      },
      {
        name: "fullscreen",
        action: SimpleMDE.toggleFullScreen,
        className: "fa fa-arrows-alt no-disable no-mobile",
        title: "Toggle Fullscreen",
        default: true
      },
      "|",
      {
        name: "guide",
        action: "https://simplemde.com/markdown-guide",
        className: "fa fa-question-circle",
        title: "Markdown Guide",
        default: true
      },
      "|",
      {
        name: "undo",
        action: SimpleMDE.undo,
        className: "fa fa-undo no-disable",
        title: "Undo"
      },
      {
        name: "redo",
        action: SimpleMDE.redo,
        className: "fa fa-repeat no-disable",
        title: "Redo"
      }


    // {
    //   name: "custom",
    //   action: function customFunction(editor){

    //     // Add your own code
    //   },
    //   className: "fa fa-star",
    //   title: "Custom Button",
    // },
    // "|", // Separator
  ],
    spellChecker: false,
    promptURLs: false,
    });

    $('.form-group input[type=date], .form-group input[type=time]').each(function (idx, elt) {
      elt.type = 'text';
      $(elt).parent().datetimepicker($(elt).data('datepicker'));
    });

        @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});

            $('.language-selector:first input').change(function(e){

              var lang = this.id;

              markdownbody.textContent = markdownbody.value;

              simplemde.value(markdownbody.value);

            });
        @endif
        });
</script>
@stop
