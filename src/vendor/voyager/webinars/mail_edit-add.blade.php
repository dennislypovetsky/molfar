@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', is_null($dataTypeContent->getKey()) ? 'Добавить письмо' : 'Изменение письма' )

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list"></i>
        {{ (is_null($dataTypeContent->getKey()) ? 'Добавить письмо' : 'Изменение письма') }}
    </h1>
@stop

@section('content')
<div class="page-content edit-add container-fluid">
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-bordered">
        <!-- form start -->
        <form role="form"
                class="form-edit-add"
                action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.webinars.update-mail', [$webinar->id, $dataTypeContent->getKey()]) }}@else{{ route('voyager.webinars.store-mail', $webinar->id) }}@endif"
                method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(!is_null($dataTypeContent->getKey()))
                {{ method_field("PUT") }}
            @endif
            <!-- CSRF TOKEN -->
            {{ csrf_field() }}
            <input type="hidden" name="webinar_id" value="{{$dataTypeContent->webinar_id}}"/>
            <div class="panel-body">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              <div class="form-group">
                <label>Вебинар</label>
                <p><a href="{{route('voyager.webinars.edit', $webinar->id)}}">{{$webinar->titleWithPersonName}}</a></p>
              </div>
              <div class="form-group">
                <label for="name">Название</label>
                <input type="text" class="form-control" name="name" value="{{(isset($dataTypeContent->name) ? $dataTypeContent->name : old('name'))}}" required/>
              </div>
              <div class="form-group">
                <label for="subject">Тема</label>
                <input type="text" class="form-control" name="subject" value="{{(isset($dataTypeContent->subject) ? $dataTypeContent->subject : old('subject'))}}" required/>
              </div>
              <div class="form-group">
                <label for="subject">Содержание</label>
                <textarea class="form-control" name="body" id="markdownbody">@if(isset($dataTypeContent->body)){{ old('body', $dataTypeContent->body) }}@else{{ old('body') }}@endif</textarea>
              </div>
            </div><!-- panel-body -->

            <div class="panel-footer">
                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
            </div>
        </form>
      </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel panel-bordered panel-dark">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="icon wb-clipboard"></i> Переменные</h3>
            </div>
           <table class="table">
             @foreach(\App\Mail\WebinarMail::varTitles() as $varKey => $varTitle)
             <tr>
              <td><span class="label label-dark">{{$varKey}}</span></td>
              <td>{{$varTitle}}</td>
             </tr>
             @endforeach
           </table>
        </div>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script>
  $('document').ready(function(){
    var markdownbody = document.getElementById("markdownbody");

  var simplemde = new SimpleMDE({
    element: markdownbody,
    forceSync: true,
    insertTexts: {
      horizontalRule: ["", "\n\n-----\n\n"],
      image: ["![Вставьте подпись к картинке сюда](Загрузите картинку в Галерею, скопируйте («Click Here» в свойствах файла) и вставьте ссылку сюда, вместо этого текста", ")"],
      link: ["[", "](http://)"],
      table: ["", "\n\n| Column 1 | Column 2 | Column 3 |\n| -------- | -------- | -------- |\n| Text     | Text      | Text     |\n\n"],
    },
    toolbar: [
      {
        name: "bold",
        action: SimpleMDE.toggleBold,
        className: "fa fa-bold",
        title: "Жирный",
        default: true
      },
      {
        name: "italic",
        action: SimpleMDE.toggleItalic,
        className: "fa fa-italic",
        title: "Курсив",
        default: true
      },
      {
        name: "strikethrough",
        action: SimpleMDE.toggleStrikethrough,
        className: "fa fa-strikethrough",
        title: "Перечеркнутый"
      },
      {
        name: "heading-1",
        action: SimpleMDE.toggleHeading1,
        className: "fa fa-header fa-header-x fa-header-1",
        title: "Заголовок"
      },
      // {
      //   name: "clean-block",
      //   action: SimpleMDE.cleanBlock,
      //   className: "fa fa-eraser fa-clean-block",
      //   title: "Clean block"
      // },
      "|",
      {
        name: "link",
        action: SimpleMDE.drawLink,
        className: "fa fa-link",
        title: "Ссылка",
        default: true
      },
      {
        name: "image",
        action: SimpleMDE.drawImage,
        className: "fa fa-picture-o",
        title: "Загрузите картинку в Галерею, скопируйте («Click Here» в свойствах файла) и вставьте ссылку",
        default: true
      },
      // {
      //   name: "table",
      //   action: SimpleMDE.drawTable,
      //   className: "fa fa-table",
      //   title: "Insert Table"
      // },
      // {
      //   name: "horizontal-rule",
      //   action: SimpleMDE.drawHorizontalRule,
      //   className: "fa fa-minus",
      //   title: "Insert Horizontal Line"
      // },
      "|",
      {
        name: "preview",
        action: SimpleMDE.togglePreview,
        className: "fa fa-eye no-disable",
        title: "Toggle Preview",
        default: true
      },
      {
        name: "side-by-side",
        action: SimpleMDE.toggleSideBySide,
        className: "fa fa-columns no-disable no-mobile",
        title: "Toggle Side by Side",
        default: true
      },
      {
        name: "fullscreen",
        action: SimpleMDE.toggleFullScreen,
        className: "fa fa-arrows-alt no-disable no-mobile",
        title: "Toggle Fullscreen",
        default: true
      },
      "|",
      {
        name: "guide",
        action: "https://simplemde.com/markdown-guide",
        className: "fa fa-question-circle",
        title: "Markdown Guide",
        default: true
      },
      "|",
      {
        name: "undo",
        action: SimpleMDE.undo,
        className: "fa fa-undo no-disable",
        title: "Undo"
      },
      {
        name: "redo",
        action: SimpleMDE.redo,
        className: "fa fa-repeat no-disable",
        title: "Redo"
      }
    ],
    spellChecker: false,
    promptURLs: false,
    });
  });
</script>
@stop
