<tr>
  <td style="font-weight: 600;">
  @if(isset($item->webinarMail)){{$item->webinarMail->name}}@else{{'–'}}@endif
  </td>
  <td>
  @if(isset($item->promocodeGroup)){{$item->promocodeGroup->name}}@else{{'–'}}@endif
  </td>
  <td>
  @if(isset($item->discount_type)){{$item->discountTypeTitle}}@else{{'–'}}@endif
  </td>
  <td>
  @if(isset($item->discount_amount)){{$item->discount_amount}}@else{{'–'}}@endif
  </td>
  <td>
  <a class="btn btn-sm btn-danger pull-right delete" data-id="{{$item->id}}" href="{{route('voyager.webinars.destroy-after-submit',[$item->webinar_id, $item->id])}}">
    <i class="voyager-trash"></i></a>
  </td>
</tr>
