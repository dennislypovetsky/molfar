@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', is_null($dataTypeContent->getKey()) ? 'Добавить группу' : 'Изменение группы' )

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list"></i>
        {{ (is_null($dataTypeContent->getKey()) ? 'Добавить группу' : 'Изменение группы') }}
    </h1>
@stop

@section('content')
<div class="page-content edit-add container-fluid">
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-bordered">
        <!-- form start -->
        <form role="form"
                class="form-edit-add"
                action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.webinar-promocodes.update-group', $dataTypeContent->getKey()) }}@else{{ route('voyager.webinar-promocodes.store-group') }}@endif"
                method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(!is_null($dataTypeContent->getKey()))
                {{ method_field("PUT") }}
            @endif

            <!-- CSRF TOKEN -->
            {{ csrf_field() }}

            <div class="panel-body">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              <div class="form-group">
                <label for="name">Название</label>
                <input type="text" class="form-control" name="name" value="{{(isset($dataTypeContent->name) ? $dataTypeContent->name : old('name'))}}" required/>
              </div>
              <div class="form-group">
                <label for="promocodes">Промокоды</label>
                <select
                  class="form-control select2-ajax"
                  name="promocodes[]" multiple
                  data-get-items-route="{{route('voyager.webinar-promocodes.relation', 'promocode')}}"
                  data-get-items-field="promocodes"
                  data-route="{{ route('voyager.webinar-promocodes.add') }}"
                  data-label="code"
                  data-error-message="{{__('voyager::bread.error_tagging')}}"
                  required
                >
                @php
                    $selected_values = isset($dataTypeContent->id) ? $dataTypeContent->promocodes()->get() : [];
                @endphp
                @foreach($selected_values as $relationshipOption)
                <option value="{{ $relationshipOption->id}}" selected="selected">{{ $relationshipOption->code }}</option>
                @endforeach
              </select>
              </div>
            </div><!-- panel-body -->

            <div class="panel-footer">
                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop

@section('javascript')
@stop
