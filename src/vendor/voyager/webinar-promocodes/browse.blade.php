@extends('voyager::master')
@section('page_title', 'Промокоды вебинаров')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-list"></i> {{ 'Промокоды вебинаров' }}
    </h1>
    <a href="{{route('voyager.webinar-promocodes.create-group')}}" class="btn btn-success btn-add-new">
      <i class="voyager-plus"></i> <span>Добавить группу</span>
    </a>
</div>
@stop

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table id="dataTable" class="table table-hover display" style="width:100%">
                    <thead>
                        <tr>
                          <th></th>
                          <th>Название</th>
                          <th>Код</th>
                          <th>Дата создания</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot style="display: block">
                      <tr>
                        <th colspan="3">
                          <form action="{{route('voyager.webinar-promocodes.add')}}" name="addPromocodeForm" id="addPromocodeForm">
                            <input type="text" class="form-control"  name="name" placeholder="Название" required>
                            <input type="text" class="form-control"  name="code" placeholder="Код (a-z,-,0-9)" maxlength="25" required>
                            <input type="submit" class="btn btn-primary"  value="Добавить">
                          </form>
                        </th>
                      </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
@stop

@section('javascript')
    <!-- DataTables -->
<script>
$(document).ready(function () {
  var rPromocode = /^[a-z0-9\-]$/;

  var table = $('#dataTable').DataTable({
    ajax: "{{route('voyager.webinar-promocodes.search')}}",
    order: [],
    processing: true,
    serverSide: true,
    drawCallback: function ( settings ) {
      var api = this.api();
      var rows = api.rows( {page:'current'} );
      var rowNodes = rows.nodes();
      var rowData = rows.data();
      var last=null;

      api.column(0, {page:'current'} ).data().each( function ( group, i ) {
        var row;
          if ( last !== group ) {
            row = $(rowNodes).eq( i );
            row.before(
              '<tr class="group"><td colspan="3" class="text-center">'
              +'<h5><span>'+group+'</span><a class="voyager-edit" style="padding-left:3px" href="webinar-promocodes/groups/'+rowData[i].groupId+'/edit"></a></h5>'
              +'</td></tr>'
            );
            last = group;
          }
      } );
    },
    language: {
      "processing": "Подождите...",
      "search": "Поиск:",
      "lengthMenu": "Показать _MENU_ записей",
      "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
      "infoEmpty": "Записи с 0 до 0 из 0 записей",
      "infoFiltered": "(отфильтровано из _MAX_ записей)",
      "infoPostFix": "",
      "loadingRecords": "Загрузка записей...",
      "zeroRecords": "Записи отсутствуют.",
      "emptyTable": "В таблице отсутствуют данные",
      "paginate": {
        "first": "Первая",
        "previous": "Предыдущая",
        "next": "Следующая",
        "last": "Последняя"
      },
      "aria": {
        "sortAscending": ": активировать для сортировки столбца по возрастанию",
        "sortDescending": ": активировать для сортировки столбца по убыванию"
      }
    },
    columns: [
      {
        data: 'groupName',
        visible: false
      },
      {data: 'name'},
      {data: 'code'},
      {
        data: 'created_at',
        searchable: false,
        render: function(timestamp){
          var date = new Date(timestamp);
          var month = date.getMonth() + 1;
          var hours = date.getHours();
          var minutes = date.getMinutes();
          return (
            date.getDate()
            +'.'
            +(month < 10 ? '0'+month : month)
            +'.'
            +date.getFullYear()
            +' '
            +(hours<10 ? '0'+hours : hours)+':'+(minutes<10?'0'+minutes:minutes)
          );
        }
      }
    ]
  });

  var addPromocodeForm = document.forms.addPromocodeForm;

  $(addPromocodeForm).submit(function(e) {
    e.preventDefault();
    var $form = $(this);
    $.post($form.prop('action'),$form.serialize())
    .done(function(data) {
      var alertType = data['alert-type'];
      var alertMessage = data['message'];
      var alerter = toastr[alertType];

      if (alerter) {
        table.draw();
        addPromocodeForm.reset();
        alerter(alertMessage);
      } else {
          toastr.error("toastr alert-type " + alertType + " is unknown");
      }
    })
    .fail(function($xhr){
      var data = $xhr.responseJSON;
      if(data.message){
        toastr.error(data.message);
      }
      if(data.errors){
        for(var field in data.errors){
          if(data.errors.hasOwnProperty(field)){
            toastr.error(data.errors[field][0]);
          }
        }
      }
    })
    return false;
  });

  addPromocodeForm.elements.code.addEventListener('keypress', function(event) {
    if (!rPromocode.test(
      String.fromCharCode(!event.charCode ? event.which : event.charCode)
    )) {
      event.preventDefault();
      return false;
    }
  });

});
</script>
@stop
