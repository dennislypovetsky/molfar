@extends('voyager::master')
@section('page_title', 'Участники вебинаров')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-people"></i> {{ 'Участники вебинаров' }}
    </h1>
</div>
@stop

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table id="dataTable" class="table table-hover display" style="width:100%">
                    <thead>
                        <tr>
                          <th></th>
                          <th>№</th>
                          <th>Статус</th>
                          <th>Вебинар</th>
                          <th>Лектор</th>
                          <th>Имя</th>
                          <th>Фамилия</th>
                          <th>Дата</th>
                          <th>Стоимость</th>
                          <th>Промокод</th>
                          <th>Телефон</th>
                          <th>Прозвон</th>
                          <th>Дозвон</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
@stop

@section('javascript')
    <!-- DataTables -->
<script>
  var rPhoneMask = /^(\d{3})(\d{2})(\d{3})(\d{2})(\d{2})$/;
function format ( d ) {
  // `d` is the original data object for the row
  return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
      '<tr>'+
          '<td>Имя:</td>'+
          '<td>'+d.firstname+' '+d.lastname+'</td>'+
      '</tr>'+
      '<tr>'+
          '<td>Почта:</td>'+
          '<td>'+d.email+'</td>'+
      '</tr>'+
      '<tr>'+
          '<td>Город:</td>'+
          '<td>'+d.city+'</td>'+
      '</tr>'+
      '<tr>'+
          '<td>Дата оплаты:</td>'+
          '<td>'+d.payment_at+'</td>'+
      '</tr>'+
      '<tr>'+
          '<td>Комментарий:</td>'+
          '<td>'+d.comment+'</td>'+
      '</tr>'+
  '</table>';
}

function renderCheckbox(name) {
  return function(currValue, type, row, meta) {
    return '<input type="checkbox" name="'+name+'" '+(currValue == 1 ? 'checked' : '')+'>';
  };
}

function renderStatus(currStatus, type, row, meta) {
  var statuses = {
    'NEW': 'Новый',
    'APPROVED': 'Подтвержден',
    'PAID': 'Оплачен',
    'CANCELED': 'Отменен',
  },
  availableOptions = {
    'NEW': ['NEW'],
    'APPROVED': ['APPROVED', 'CANCELED'],
    'PAID': ['PAID', 'CANCELED'],
  },
  html = '', status;

  if(parseFloat(row.total_price) == 0){
    availableOptions['NEW'].push('APPROVED');
  }
  availableOptions['NEW'].push('CANCELED');

  if(currStatus == 'CANCELED') {
    return statuses[currStatus];
  }
  html+= '<select size="1" name="status">';
    for(var i=0, len=availableOptions[currStatus].length; i<len; i++){
      status = availableOptions[currStatus][i];
      html+= '<option value="'+status+'" '+(status == currStatus ? 'selected="selected' : '')+'">'+statuses[status]+'</option>';
    }
  html+= '</select>';
  return html;
}
$(document).ready(function () {

  var table = $('#dataTable').DataTable({
    ajax: "{{route('voyager.webinar-participants.search')}}",
    order: [],
    processing: true,
    serverSide: true,
    language: {
      "processing": "Подождите...",
      "search": "Поиск:",
      "lengthMenu": "Показать _MENU_ записей",
      "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
      "infoEmpty": "Записи с 0 до 0 из 0 записей",
      "infoFiltered": "(отфильтровано из _MAX_ записей)",
      "infoPostFix": "",
      "loadingRecords": "Загрузка записей...",
      "zeroRecords": "Записи отсутствуют.",
      "emptyTable": "В таблице отсутствуют данные",
      "paginate": {
        "first": "Первая",
        "previous": "Предыдущая",
        "next": "Следующая",
        "last": "Последняя"
      },
      "aria": {
        "sortAscending": ": активировать для сортировки столбца по возрастанию",
        "sortDescending": ": активировать для сортировки столбца по убыванию"
      }
    },
    // columnDefs: [{"targets": -1, "searchable":  true, "orderable": true}]
    columns: [
      {
        className: 'details-control',
        searchable: false,
        orderable:  false,
        data: null,
        defaultContent: ''
      },
      {"data": "reference"},
      {
        data: 'status',
        searchable: false,
        orderData: [ 2, 7 ],
        render: renderStatus
      },
      { "data": "webinarTitle" },
      { "data": "webinarPersonName" },
      { data: 'firstname', orderData: [ 5, 6 ]},
      { data: 'lastname', orderData: [ 5, 6 ]},
      {
        data: 'created_at',
        searchable: false,
        render: function(timestamp){
          var date = new Date(timestamp);
          var month = date.getMonth() + 1;
          var hours = date.getHours();
          var minutes = date.getMinutes();
          return ((date.getDate())+'.'+(month < 10 ? '0'+month : month)+' '
            +(hours<10 ? '0'+hours : hours)+':'+(minutes<10?'0'+minutes:minutes));
        }
      },
      { data: 'total_price'},
      { data: 'promocode'},
      {
        data: 'phone',
        render: function(phone){
          return '<a href="tel:'+phone+'" style="white-space:nowrap">+'+phone.replace(rPhoneMask, '$1 ($2) $3-$4-$5')+'</a>';
        }
      },
      {
        data: 'is_made_call',
        searchable: false,
        render: renderCheckbox('is_made_call')
      },
      {
        data: 'is_answered_call',
        searchable: false,
        render: renderCheckbox('is_answered_call')
      },
    ]
  });

  $('#dataTable tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row( tr );

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    } else {
        // Open this row
        row.child( format(row.data()) ).show();
        tr.addClass('shown');
    }
  })
  .on('change', 'select[name="status"], input[type="checkbox"]', function(){
    var $this = $(this);
    var row = table.row( $this.closest('tr') );

    var postData = {
      id: (row.data()).id
    };
    postData[this.name] = (this.type == 'checkbox'? (this.checked ? 1 : 0) : $this.val());

    $.post("{{route('voyager.webinar-participants.update')}}", postData)
    .done(function(data) {
      var alertType = data['alert-type'];
      var alertMessage = data['message'];
      var alerter = toastr[alertType];

      if (alerter) {
        table.draw();
          alerter(alertMessage);
      } else {
          toastr.error("toastr alert-type " + alertType + " is unknown");
      }
    });
  })

});
</script>
@stop
